package pl.edu.pjatk.murek.grafik;

import org.junit.Test;

import pl.edu.pjatk.murek.grafik.utils.DateUtils;

import static org.junit.Assert.assertEquals;

/**
 * Created on 2017-04-26.
 */

public class DateUtilsTest {

    @Test
    public void convertToDateWithZeroTrue() throws Exception {
        assertEquals("2017-00-01", DateUtils.convertToDateWithZero(2017,0,1));
        assertEquals("2017-01-01", DateUtils.convertToDateWithZero(2017,1,1));
        assertEquals("2017-09-12", DateUtils.convertToDateWithZero(2017,9,12));
        assertEquals("2017-10-09", DateUtils.convertToDateWithZero(2017,10,9));
        assertEquals("2017-12-12", DateUtils.convertToDateWithZero(2017,12,12));
    }
}
