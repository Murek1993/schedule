package pl.edu.pjatk.murek.grafik.models;

/**
 * Created on 2017-04-18.
 */

public enum Statuses {
    DONE("Wykonane"), NOT_DONE("Nie wykonane"), VERIFIED("Zweryfikowane"), FOR_VERIFICATION("Do weryfikacji"), IN_PROGRESS("W trakcie"), REJECTED("Odrzucone"), PLANNED("Zaplanowane");

    private final String status;

    Statuses(final String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }

}
