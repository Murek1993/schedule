package pl.edu.pjatk.murek.grafik.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import com.andexert.expandablelayout.library.ExpandableLayoutListView;
import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.adapters.TaskListAdapter;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.TaskData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-05-03.
 */


@Setter
@EFragment(R.layout.tasks_list_layout)
public class TasksFragment extends Fragment implements Callback<List<TaskData>> {

    @ViewById(R.id.expandable_list_view_tasks)
    protected ExpandableLayoutListView listView;

    @ViewById(R.id.progress_view_tasks)
    protected CircularProgressView progressView;

    @ViewById(R.id.swipe_refresh_layout_tasks)
    protected SwipeRefreshLayout swipeRefreshLayout;

    @ViewById(R.id.btn_add_task)
    protected AddFloatingActionButton addTaskBtn;

    private FragmentManager fragmentManager;

    private static Context context;

    public static void setContext(Context context) {
        TasksFragment.context = context;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tasks_list_layout, container, false);
    }


    @AfterViews
    protected void afterViews() {
        ((DrawerActivity) context).getToolbar().setTitle(R.string.add_task);
        fragmentManager = ((DrawerActivity) context).getSupportFragmentManager();
        swipeRefreshLayout.setColorSchemeResources(R.color.colorMenuUp);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getTaskList();
            }
        });

        addTaskBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddTaskFragment_ addTaskFragment = new AddTaskFragment_();
                AddTaskFragment_.setContext(context);
                fragmentManager.beginTransaction()
                        .replace(R.id.container_drawer, addTaskFragment).addToBackStack(null)
                        .commit();
            }
        });


        listView.setOnScrollListener((new ExpandableLayoutListView(context)).new OnExpandableLayoutScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listView.getChildAt(0) != null) {
                    swipeRefreshLayout.setEnabled(listView.getFirstVisiblePosition() == 0 && listView.getChildAt(0).getTop() == 0);
                }
            }
        });

        getTaskList();
    }

    private void getTaskList() {
        progressView.startAnimation();

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<List<TaskData>> call = apiService.getTaskList(userSession.getIdMyGroup());
        call.enqueue(this);
    }


    @Override
    public void onResponse(Response<List<TaskData>> response, Retrofit retrofit) {
        disableRefreshEffect();

        if (response.body() != null) {
            TaskListAdapter adapter = new TaskListAdapter(context, response.body(), fragmentManager);
            listView.setAdapter(adapter);
        } else {
            Toast.makeText(context, R.string.toast_get_tasks_list_fail, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        disableRefreshEffect();

        Toast.makeText(context, R.string.toast_server_fail, Toast.LENGTH_SHORT).show();
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }
}
