package pl.edu.pjatk.murek.grafik.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import lombok.Getter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.adapters.LoginAdapter;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;

@EActivity
@Getter
public class MainActivity extends AppCompatActivity {

    @ViewById(R.id.tab_layout)
    protected TabLayout tabLayout;

    @ViewById(R.id.pager)
    protected ViewPager viewPager;

    private static final int CODE_INTERNET = 106;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.INTERNET, Manifest.permission.ACCESS_NETWORK_STATE}, CODE_INTERNET);
        } else {
            skipLoginPageIfUserIsLogged();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_view);
    }

    private void skipLoginPageIfUserIsLogged(){
        if (MySharedPreferencesUtils.isLogged(MainActivity.this)) {
            Intent intent = new Intent(MainActivity.this, DrawerActivity_.class);
            startActivity(intent);
            this.finish();
        }
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        System.exit(0);
    }

    @AfterViews
    public void afterViews() {
        setTabs();
        setViewPager();
    }

    private void setTabs() {
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_login));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_registration));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void setViewPager() {
        final LoginAdapter adapter = new LoginAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), MainActivity.this);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == CODE_INTERNET) {
            if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                finishAffinity();
                 System.exit(0);
            } else {
                skipLoginPageIfUserIsLogged();
            }
        }
    }

}
