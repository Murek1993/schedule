package pl.edu.pjatk.murek.grafik.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.squareup.okhttp.ResponseBody;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity_;
import pl.edu.pjatk.murek.grafik.adapters.CommentsListAdapter;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.Comment;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-19.
 */
@Setter
@EFragment(R.layout.comments_layout)
public class CommentsFragment extends Fragment implements Callback<List<Comment>>, View.OnClickListener {

    @ViewById(R.id.comments_list_view)
    protected ListView listView;

    @ViewById(R.id.add_comment_editText)
    protected EditText addCommentET;

    @ViewById(R.id.add_comment_button)
    protected ImageButton addCommentButton;

    @ViewById(R.id.progress_view_comments)
    protected CircularProgressView progressView;

    @ViewById(R.id.swipe_refresh_layout_comments)
    protected SwipeRefreshLayout swipeRefreshLayout;

    private boolean ifAction;

    private int idCleaningTask;

    private FragmentManager fragmentManager;

    private static Context context;

    public static void setContext(Context context) {
        CommentsFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.comments_layout, container, false);
    }

    @AfterViews
    protected void afterViews() {
        ((DrawerActivity) context).getToolbar().setTitle(R.string.comments);
        fragmentManager = ((DrawerActivity) context).getSupportFragmentManager();
        addCommentButton.setOnClickListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorMenuUp);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCommentsList();
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listView.getChildAt(0) != null) {
                    swipeRefreshLayout.setEnabled(listView.getFirstVisiblePosition() == 0 && listView.getChildAt(0).getTop() == 0);
                }
            }
        });

        getCommentsList();
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void getCommentsList() {
        progressView.startAnimation();

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Bundle arguments = getArguments();
        idCleaningTask = arguments.getInt("idCleaningTask");
        ifAction = arguments.getBoolean("ifAction");

        if (!ifAction) {
            addCommentButton.setEnabled(false);
            addCommentET.setEnabled(false);
        }

        Call<List<Comment>> call = apiService.getComments(idCleaningTask);
        call.enqueue(this);

    }


    @Override
    public void onResponse(Response<List<Comment>> response, Retrofit retrofit) {
        disableRefreshEffect();

        if (response.body() != null) {
            CommentsListAdapter adapter = new CommentsListAdapter(context, response.body());
            listView.setAdapter(adapter);
        } else {
            Toast.makeText(context, getString(R.string.toast_download_comments_fail), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        disableRefreshEffect();

        Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        String comment = addCommentET.getText().toString();

        if (ifCorrectData(comment)) {
            progressView.startAnimation();
            addCommentButton.setEnabled(false);

            ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
            Session session = MySharedPreferencesUtils.getSession((Activity) context);
            Call<ResponseBody> call = apiService.addComment(comment, idCleaningTask, session.getIdAccount());
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                    disableRefreshEffect();
                    addCommentButton.setEnabled(true);

                    if (response.code() == 200) {
                        refresh();
                    } else {
                        ResponseBody responseBody = response.errorBody();
                        MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.toast_add_comment_fail));
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    disableRefreshEffect();
                    addCommentButton.setEnabled(true);

                    Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private boolean ifCorrectData(String comment) {
        if (comment.equals("")) {
            Toast.makeText(context, getString(R.string.toast_enter_comment), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void refresh() {
        CommentsFragment_ commentsFragment = new CommentsFragment_();
        CommentsFragment_.setContext(context);
        Bundle arguments = new Bundle();
        arguments.putInt("idCleaningTask", idCleaningTask);
        arguments.putBoolean("ifAction", ifAction);
        commentsFragment.setArguments(arguments);
        fragmentManager.beginTransaction()
                .replace(R.id.container_drawer, commentsFragment).addToBackStack(null)
                .commit();
        DrawerActivity_.setCurrentFragment(commentsFragment);
    }

}
