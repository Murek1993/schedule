package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */
@Getter
@Setter
public class EditGroupData {

    @SerializedName("NazwaGrupy")
    private String groupName;

    @SerializedName("HasloGrupy")
    private String groupPassword;

    @SerializedName("DataPierwszegoSprzatania")
    private String firstCleaningData;

    @SerializedName("CoIleDniSprzatanie")
    private int howManyDays;

    @SerializedName("ProcentZwrotuPunktow")
    private int percentageReturn;

    @SerializedName("JestAktywna")
    private boolean ifActive;

    @SerializedName("CzyRuszyla")
    private boolean ifStarted;

    public EditGroupData(String groupName, String groupPassword, String firstCleaningData, int howManyDays, int percentageReturn, boolean ifActive, boolean ifStarted) {
        this.groupName = groupName;
        this.groupPassword = groupPassword;
        this.firstCleaningData = firstCleaningData;
        this.howManyDays = howManyDays;
        this.percentageReturn = percentageReturn;
        this.ifActive = ifActive;
        this.ifStarted = ifStarted;
    }
}
