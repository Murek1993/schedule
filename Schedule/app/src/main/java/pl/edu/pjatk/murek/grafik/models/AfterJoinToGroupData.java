package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-05-13.
 */

@Getter
@Setter
public class AfterJoinToGroupData {
    @SerializedName("IdGrupy")
    private int idGroup;

    @SerializedName("CzyAktywnaGrupa")
    private boolean ifActiveGroup;

    public AfterJoinToGroupData(int idGroup, boolean ifActiveGroup) {
        this.idGroup = idGroup;
        this.ifActiveGroup = ifActiveGroup;
    }
}
