package pl.edu.pjatk.murek.grafik.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.squareup.okhttp.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity_;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.fragments.CommentsFragment;
import pl.edu.pjatk.murek.grafik.models.TaskDataWithStatus;
import pl.edu.pjatk.murek.grafik.models.Statuses;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.DownloadRecordingUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-21.
 */

public class TasksRatedListAdapter extends ArrayAdapter<TaskDataWithStatus> {
    private List<TaskDataWithStatus> list;
    private Context context;
    private boolean ifAction;
    private FragmentManager fragmentManager;
    private TextView taskName;

    public TasksRatedListAdapter(Context context, List<TaskDataWithStatus> list, boolean ifAction, FragmentManager fragmentManager) {
        super(context, android.R.layout.simple_list_item_1, list);
        this.list = list != null? list : new ArrayList<TaskDataWithStatus>();
        this.context = context;
        this.ifAction = ifAction;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.tasks_rated_list_row, parent, false);
        }

        taskName = (TextView) convertView.findViewById(R.id.task_rated_header_text);
        taskName.setText(list.get(position).getName());

        TextView content = (TextView) convertView.findViewById(R.id.task_rated_description);
        content.setText(list.get(position).getDescription());


        String downloadedPhoto = list.get(position).getPhoto();
        if (downloadedPhoto != null) {
            byte[] photoByteSize = Base64.decode(downloadedPhoto, Base64.DEFAULT);
            ImageView photoTask = (ImageView) convertView.findViewById(R.id.photo_task_rated);
            Bitmap bmp = BitmapFactory.decodeByteArray(photoByteSize, 0, photoByteSize.length);

            photoTask.setImageBitmap(bmp);
        } else {
            ImageView photoTask = (ImageView) convertView.findViewById(R.id.photo_task_rated);
            photoTask.getLayoutParams().height = 0;
            photoTask.getLayoutParams().width = 0;
        }

        TextView pointsTV = (TextView) convertView.findViewById(R.id.tv_points_rated);
        int points = list.get(position).getPoints();
        pointsTV.setText("$" + points);

        CircleImageView iconView = (CircleImageView) convertView.findViewById(R.id.circleView_icon);
        String iconName = list.get(position).getIconName();
        if (iconName != null && iconName.contains("icon")) {
            int resID = context.getResources().getIdentifier(iconName, "mipmap", context.getPackageName());
            if (resID != 0) {
                iconView.setImageResource(resID);
            }
        }


        ImageButton speaker = (ImageButton) convertView.findViewById(R.id.imageButton_speaker);
        final String recording = list.get(position).getRecording();
        if (recording == null) {
            speaker.setVisibility(View.INVISIBLE);
        } else {
            speaker.setVisibility(View.VISIBLE);
            speaker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DownloadRecordingUtils.playSound(recording, context);
                }
            });
        }

        final ShineButton up = (ShineButton) convertView.findViewById(R.id.btn_up);
        final ShineButton down = (ShineButton) convertView.findViewById(R.id.btn_down);

        Integer verificationStatusFromDatabase = list.get(position).getVerificationStatus();
        if (verificationStatusFromDatabase != null) {
            Statuses verificationStatus = Statuses.values()[verificationStatusFromDatabase];
            if (verificationStatus.equals(Statuses.VERIFIED)) {
                up.setChecked(true);
            } else if (verificationStatus.equals(Statuses.REJECTED)) {
                down.setChecked(true);
            }
        }
        AddFloatingActionButton plus = (AddFloatingActionButton) convertView.findViewById(R.id.add_comment);
        final int idCleaningTask = list.get(position).getIdCleaningTask();

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentsFragment commentsFragment = new CommentsFragment();
                CommentsFragment.setContext(context);
                Bundle arguments = new Bundle();
                arguments.putInt("idCleaningTask", idCleaningTask);
                arguments.putBoolean("ifAction", ifAction);
                commentsFragment.setArguments(arguments);
                fragmentManager.beginTransaction()
                        .replace(R.id.container_drawer, commentsFragment).addToBackStack(null)
                        .commit();
                DrawerActivity_.setCurrentFragment(commentsFragment);
            }
        });

        if (ifAction) {
            Statuses performedStatusFromDatabase = Statuses.values()[list.get(position).getPerformedStatus()];
            if (performedStatusFromDatabase.equals(Statuses.DONE)) {
                up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (up.isChecked()) {
                            down.setChecked(false);
                            changeRatedStatus(idCleaningTask, Statuses.VERIFIED, up);
                        }
                    }
                });

                down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (down.isChecked()) {
                            up.setChecked(false);
                            changeRatedStatus(idCleaningTask, Statuses.REJECTED, down);
                        }
                    }
                });
            } else {
                up.setVisibility(View.INVISIBLE);
                down.setVisibility(View.INVISIBLE);
            }
        } else {
            up.setEnabled(false);
            down.setEnabled(false);
        }


        return convertView;
    }

    private void changeRatedStatus(int idCleaningTask, Statuses status, final ShineButton shineButton) {
        shineButton.setEnabled(false);
        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Call<ResponseBody> call = apiService.rateTask(idCleaningTask, status.ordinal());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                shineButton.setEnabled(true);
                if (response.code() != 200) {
                    ResponseBody responseBody = response.errorBody();
                    MessageUtils.showErrorMessagge(context, responseBody, context.getString(R.string.dialog_change_task_rated_fail));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                shineButton.setEnabled(true);
                Toast.makeText(context, context.getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
