package pl.edu.pjatk.murek.grafik.utils;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created on 2017-04-22.
 */

public class DateUtils {

    public static String convertToDateWithMinutes(DateTime dateTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        return sdf.format(dateTime.toDate());
    }

    public static Date convertStringToDateTime(String dateString) {
        Date date = null;
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            return sdf.parse(dateString);
        }
        catch (Exception e){
            e.printStackTrace();
            try{
                SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
                return sdf.parse(dateString);
            }
            catch (Exception e1){
                e1.printStackTrace();
            }
        }
        return date;
    }

    /**
     * Format 2017-01-01
     */
    public static String convertToDateWithZero(int year, int month, int day) {
        String formattedDay = (String.valueOf(day));
        String formattedMonth = (String.valueOf(month));

        if (day < 10) {
            formattedDay = "0" + day;
        }

        if (month < 10) {
            formattedMonth = "0" + month;
        }
        return year + "-" + formattedMonth + "-" + formattedDay;
    }

    public static String formatDate(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
}
