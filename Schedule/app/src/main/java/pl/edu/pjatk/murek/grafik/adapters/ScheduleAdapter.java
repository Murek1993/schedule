package pl.edu.pjatk.murek.grafik.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import pl.edu.pjatk.murek.grafik.fragments.SchedulePartnerGroupFragment;
import pl.edu.pjatk.murek.grafik.fragments.ScheduleMyGroupFragment;
import pl.edu.pjatk.murek.grafik.fragments.ScheduleMyGroupFragment_;
import pl.edu.pjatk.murek.grafik.fragments.SchedulePartnerGroupFragment_;

/**
 * Created on 2017-04-29.
 */

public class ScheduleAdapter extends FragmentStatePagerAdapter {
    private int tabsNumber;

    private static Context context;

    private ScheduleMyGroupFragment_ scheduleMyGroupFragment;
    private SchedulePartnerGroupFragment_ schedulePartnerGroupFragment;

    private boolean myGroupIsActive;
    private boolean partnerGroupIsActive;

    public ScheduleAdapter(FragmentManager fm, int tabsNumber, Context con, boolean myGroupIsActive, boolean partnerGroupIsActive) {
        super(fm);
        this.tabsNumber = tabsNumber;
        context = con;
        this.myGroupIsActive = myGroupIsActive;
        this.partnerGroupIsActive = partnerGroupIsActive;
        if (myGroupIsActive) {
            scheduleMyGroupFragment = new ScheduleMyGroupFragment_();
            ScheduleMyGroupFragment.setContext(context);
        }
        if (partnerGroupIsActive) {
            schedulePartnerGroupFragment = new SchedulePartnerGroupFragment_();
            SchedulePartnerGroupFragment.setContext(context);
        }
    }

    @Override
    public Fragment getItem(int position) {
        if (myGroupIsActive && partnerGroupIsActive) {
            switch (position) {
                case 0:
                    return scheduleMyGroupFragment;
                case 1:
                    return schedulePartnerGroupFragment;
                default:
                    return null;
            }
        } else if (myGroupIsActive) {
            switch (position) {
                case 0:
                    return scheduleMyGroupFragment;
                default:
                    return null;
            }
        } else {
            switch (position) {
                case 0:
                    return schedulePartnerGroupFragment;
                default:
                    return null;
            }
        }
    }

    @Override
    public int getCount() {
        return tabsNumber;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }
}
