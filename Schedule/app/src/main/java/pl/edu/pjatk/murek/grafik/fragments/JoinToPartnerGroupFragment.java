package pl.edu.pjatk.murek.grafik.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.andexert.expandablelayout.library.ExpandableLayout;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.squareup.okhttp.ResponseBody;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.apache.commons.lang3.SerializationUtils;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.JoinToGroupData;
import pl.edu.pjatk.murek.grafik.models.AfterJoinToGroupData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-19.
 */
@Setter
@Getter
@EFragment(R.layout.join_to_partner_group_layout)
public class JoinToPartnerGroupFragment extends Fragment implements Callback<JoinToGroupData>, NfcAdapter.CreateNdefMessageCallback, NfcAdapter.OnNdefPushCompleteCallback {

    private static final int CODE_NFC_PREPARE = 103;
    private static final int CODE_NFC_TURN_ON = 104;

    @ViewById(R.id.et_group_name_join)
    protected EditText groupNameET;

    @ViewById(R.id.et_password_join)
    protected EditText passwordET;

    @ViewById(R.id.btn_join_nfc)
    protected Button btn;

    @ViewById(R.id.btn_password_join)
    protected Button btnJoin;

    @ViewById(R.id.progress_view_join)
    protected CircularProgressView progressView;

    @ViewById(R.id.nfcEL)
    protected ExpandableLayout expandableLayoutNFC;

    public NfcAdapter nfcAdapter;
    private static Context context;

    protected JoinToGroupData groupData;

    public static void setContext(Context context) {
        JoinToPartnerGroupFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.join_to_partner_group_layout, container, false);
    }

    private void prepareNFC() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(context);

        if (nfcAdapter == null) {
            expandableLayoutNFC.getHeaderLayout().setEnabled(false);

            Toast.makeText(context,
                    getString(R.string.toast_nfc_detect_fail),
                    Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context,
                    getString(R.string.toast_nfc_detect_success),
                    Toast.LENGTH_LONG).show();
            nfcAdapter.setNdefPushMessageCallback(JoinToPartnerGroupFragment.this, (Activity) context);
            nfcAdapter.setOnNdefPushCompleteCallback(JoinToPartnerGroupFragment.this, (Activity) context);
        }
    }

    @AfterViews
    protected void afterViews() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.NFC) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.NFC}, CODE_NFC_PREPARE);
        } else {
            prepareNFC();
        }
        ((DrawerActivity) context).getToolbar().setTitle(R.string.join_to_partner_group);
        sendRequestGetGroupData();
    }

    @Click(R.id.btn_password_join)
    protected void joinToGroupPasswordClick() {
        String groupName = groupNameET.getText().toString();
        String password = passwordET.getText().toString();
        sendRequestGetGroupData();
        if (ifCorrectData(groupName, password)) {
            joinToGroupPassword(groupName, password);
        }

    }

    @Click(R.id.btn_join_nfc)
    protected void joinToGroupNFCClick() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.NFC) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.NFC}, CODE_NFC_TURN_ON);
        } else {
            turnOnNFC();
        }
    }

    private void turnOnNFC() {
        final Intent intent = new Intent();

        if (NfcAdapter.getDefaultAdapter(context).isEnabled()) {
            intent.setAction(Settings.ACTION_NFCSHARING_SETTINGS);
        } else {
            intent.setAction(Settings.ACTION_NFC_SETTINGS);
        }

        startActivity(intent);
    }

    private void joinToGroupPassword(String groupName, String password) {

        JoinToGroupData groupData = new JoinToGroupData(groupName, password);
        sendRequestJoinToGroup(groupData);
    }

    private boolean ifCorrectData(String groupName, String password) {

        return validationOnEmpty(groupName, password);
    }

    private boolean validationOnEmpty(String groupName, String password) {
        if (groupName.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_group_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_password), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public void clearForm() {
        groupNameET.getText().clear();
        passwordET.getText().clear();
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
        btnJoin.setEnabled(true);
    }

    public void enableRefreshEffect() {

        progressView.startAnimation();
        progressView.setVisibility(View.VISIBLE);
        btnJoin.setEnabled(false);
    }

    public void sendRequestJoinToGroup(final JoinToGroupData joinToGroupData) {
        if (progressView != null && btnJoin != null) {
            enableRefreshEffect();
        }

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<AfterJoinToGroupData> call = apiService.joinToGroup(joinToGroupData, userSession.getIdAccount());
        call.enqueue(new Callback<AfterJoinToGroupData>() {
            @Override
            public void onResponse(Response<AfterJoinToGroupData> response, Retrofit retrofit) {
                if (progressView != null && btnJoin != null) {
                    disableRefreshEffect();
                }


                if (response.code() == 200) {
                    Toast.makeText(context, getString(R.string.toast_join_to_group_success), Toast.LENGTH_LONG).show();

                    Session session = MySharedPreferencesUtils.getSession((Activity) context);
                    AfterJoinToGroupData afterJoinToGroupData = response.body();
                    session.setIdPartnerGroup(afterJoinToGroupData.getIdGroup());
                    session.setPartnerGroupIsActive(afterJoinToGroupData.isIfActiveGroup());
                    MySharedPreferencesUtils.saveSession(session, (Activity) context);

                    ((DrawerActivity) context).restartDrawerActivity();

                } else {

                    ResponseBody responseBody = response.errorBody();
                    MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.toast_join_to_group_fail));
                }

            }

            @Override
            public void onFailure(Throwable t) {
                if (progressView != null && btnJoin != null) {
                    disableRefreshEffect();
                }

                Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void sendRequestGetGroupData() {

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<JoinToGroupData> call1 = apiService.getJoinToGroupData(userSession.getIdMyGroup(), userSession.getIdAccount());
        call1.enqueue(this);
    }

    @Override
    public void onNdefPushComplete(NfcEvent event) {
        final String eventString = getString(R.string.sent_name_and_group_password);
        ((Activity) context).runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(context,
                        eventString,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        byte[] data = SerializationUtils.serialize(groupData);
        NdefRecord ndefRecordOut = new NdefRecord(
                NdefRecord.TNF_MIME_MEDIA,
                "text/plain".getBytes(),
                new byte[]{},
                data);

        NdefMessage ndefMessageout = new NdefMessage(ndefRecordOut);
        return ndefMessageout;
    }

    @Override
    public void onResponse(Response<JoinToGroupData> response, Retrofit retrofit) {
        clearForm();

        if (response.code() == 200) {
            groupData = response.body();

        } else {
            ResponseBody responseBody = response.errorBody();
            MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.toast_get_group_data_fail));
        }

    }

    @Override
    public void onFailure(Throwable t) {
        Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == CODE_NFC_PREPARE) {
            prepareNFC();
        } else if (requestCode == CODE_NFC_TURN_ON) {
            turnOnNFC();
        }
    }


}
