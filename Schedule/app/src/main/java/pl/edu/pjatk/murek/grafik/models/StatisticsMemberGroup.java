package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */
@Getter
@Setter
public class StatisticsMemberGroup {

    @SerializedName("Login")
    private String login;

    @SerializedName("Punkty")
    private long points;

    @SerializedName("Posprzatania")
    private int cleanup;

    @SerializedName("NiePosprzatania")
    private int noCleanup;

    @SerializedName("Zdjecie")
    private String photo;

    public StatisticsMemberGroup(String login, long points, int cleanup, int noCleanup, String photo) {
        this.login = login;
        this.points = points;
        this.cleanup = cleanup;
        this.noCleanup = noCleanup;
        this.photo = photo;
    }
}
