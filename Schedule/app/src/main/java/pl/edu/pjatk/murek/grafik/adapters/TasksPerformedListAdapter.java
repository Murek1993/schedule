package pl.edu.pjatk.murek.grafik.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.AddFloatingActionButton;
import com.squareup.okhttp.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity_;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.fragments.CommentsFragment;
import pl.edu.pjatk.murek.grafik.models.TaskDataWithStatus;
import pl.edu.pjatk.murek.grafik.models.Statuses;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.DownloadRecordingUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-21.
 */

public class TasksPerformedListAdapter extends ArrayAdapter<TaskDataWithStatus> {
    private List<TaskDataWithStatus> list;
    private Context context;
    private boolean ifAction;
    private FragmentManager fragmentManager;

    public TasksPerformedListAdapter(Context context, List<TaskDataWithStatus> list, boolean ifAction, FragmentManager fragmentManager) {
        super(context, android.R.layout.simple_list_item_1, list);
        this.list = list != null? list : new ArrayList<TaskDataWithStatus>();
        this.context = context;
        this.ifAction = ifAction;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.tasks_performed_list_row, parent, false);
        }

        TextView taskName = (TextView) convertView.findViewById(R.id.task_performed_header_text);
        taskName.setText(list.get(position).getName());

        final TextView content = (TextView) convertView.findViewById(R.id.task_performed_description);
        content.setText(list.get(position).getDescription());

        String downloadedPhoto = list.get(position).getPhoto();
        if (downloadedPhoto != null) {
            byte[] photoByteSize = Base64.decode(downloadedPhoto, Base64.DEFAULT);
            ImageView photoTask = (ImageView) convertView.findViewById(R.id.photo_task_performed);

            Bitmap bmp = BitmapFactory.decodeByteArray(photoByteSize, 0, photoByteSize.length);
            photoTask.setImageBitmap(bmp);
        } else {
            ImageView photoTask = (ImageView) convertView.findViewById(R.id.photo_task_performed);
            photoTask.getLayoutParams().height = 0;
            photoTask.getLayoutParams().width = 0;
        }

        TextView pointsTV = (TextView) convertView.findViewById(R.id.tv_points_performed);
        int points = list.get(position).getPoints();
        pointsTV.setText("$" + points);

        CircleImageView iconView = (CircleImageView) convertView.findViewById(R.id.circleView_icon);
        String iconName = list.get(position).getIconName();
        if (iconName != null && iconName.contains("icon")) {
            int resID = context.getResources().getIdentifier(iconName, "mipmap", context.getPackageName());
            if (resID != 0) {
                iconView.setImageResource(resID);
            }
        }

        ImageButton speaker = (ImageButton) convertView.findViewById(R.id.imageButton_speaker);
        final String recording = list.get(position).getRecording();
        if (recording == null) {
            speaker.setVisibility(View.INVISIBLE);
        } else {
            speaker.setVisibility(View.VISIBLE);
            speaker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DownloadRecordingUtils.playSound(recording, context);
                }
            });
        }

        final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox_performed);
        Statuses performedStatus = Statuses.values()[list.get(position).getPerformedStatus()];
        if (performedStatus.equals(Statuses.DONE)) {
            checkBox.setChecked(true);
        }

        AddFloatingActionButton plus = (AddFloatingActionButton) convertView.findViewById(R.id.add_comment);
        final int idCleaningTask = list.get(position).getIdCleaningTask();

        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentsFragment commentsFragment = new CommentsFragment();
                CommentsFragment.setContext(context);
                Bundle arguments = new Bundle();
                arguments.putInt("idCleaningTask", idCleaningTask);
                arguments.putBoolean("ifAction", ifAction);
                commentsFragment.setArguments(arguments);
                fragmentManager.beginTransaction()
                        .replace(R.id.container_drawer, commentsFragment).addToBackStack(null)
                        .commit();
                DrawerActivity_.setCurrentFragment(commentsFragment);
            }
        });

        if (ifAction) {

            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        changePerformedStatus(idCleaningTask, Statuses.DONE, checkBox);
                        checkBox.setBackgroundColor(Color.TRANSPARENT);
                    } else {
                        changePerformedStatus(idCleaningTask, Statuses.NOT_DONE, checkBox);
                    }
                }
            });


        } else {
            checkBox.setEnabled(false);
        }


        return convertView;
    }


    private void changePerformedStatus(int idCleaningTask, Statuses status, final CheckBox checkBox) {
        checkBox.setEnabled(false);
        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Call<ResponseBody> call = apiService.confirmTask(idCleaningTask, status.ordinal());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                checkBox.setEnabled(true);
                if (response.code() != 200) {
                    ResponseBody responseBody = response.errorBody();
                    MessageUtils.showErrorMessagge(context, responseBody, context.getString(R.string.dialog_change_performed_status_fail));
                }
            }

            @Override
            public void onFailure(Throwable t) {
                checkBox.setEnabled(true);
                Toast.makeText(context, context.getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
            }
        });
    }


}
