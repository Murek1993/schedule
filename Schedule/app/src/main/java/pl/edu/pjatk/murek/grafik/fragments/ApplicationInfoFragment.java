package pl.edu.pjatk.murek.grafik.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EFragment;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;

/**
 * Created on 2017-05-08.
 */
@Setter
@EFragment(R.layout.application_info_layout)
public class ApplicationInfoFragment extends Fragment {
    private static Context context;

    public static void setContext(Context context) {
        ApplicationInfoFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.application_info_layout, container, false);
    }

}
