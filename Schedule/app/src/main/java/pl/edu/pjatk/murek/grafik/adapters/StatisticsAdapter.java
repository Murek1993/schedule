package pl.edu.pjatk.murek.grafik.adapters;

import android.content.Context;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import pl.edu.pjatk.murek.grafik.fragments.StatisticsMyGroupFragment;
import pl.edu.pjatk.murek.grafik.fragments.StatisticsMyGroupFragment_;
import pl.edu.pjatk.murek.grafik.fragments.StatisticsPartnerGroupFragment;
import pl.edu.pjatk.murek.grafik.fragments.StatisticsPartnerGroupFragment_;

/**
 * Created on 2017-04-29.
 */

public class StatisticsAdapter extends FragmentStatePagerAdapter {
    private int tabsNumber;

    private static Context context;

    private StatisticsMyGroupFragment_ statisticsMyGroupFragment;
    private StatisticsPartnerGroupFragment_ statisticsPartnerGroupFragment;
    private boolean myGroupIsActive;
    private boolean partnerGroupIsActive;

    public StatisticsAdapter(FragmentManager fm, int tabsNumber, Context con, boolean myGroupIsActive, boolean partnerGroupIsActive) {
        super(fm);
        this.tabsNumber = tabsNumber;
        context = con;
        this.myGroupIsActive = myGroupIsActive;
        this.partnerGroupIsActive = partnerGroupIsActive;
        if (myGroupIsActive) {
            statisticsMyGroupFragment = new StatisticsMyGroupFragment_();
            StatisticsMyGroupFragment.setContext(context);
        }
        if (partnerGroupIsActive) {
            statisticsPartnerGroupFragment = new StatisticsPartnerGroupFragment_();
            StatisticsPartnerGroupFragment.setContext(context);
        }
    }

    @Override
    public Fragment getItem(int position) {
        if (myGroupIsActive && partnerGroupIsActive) {
            switch (position) {
                case 0:
                    return statisticsMyGroupFragment;
                case 1:
                    return statisticsPartnerGroupFragment;
                default:
                    return null;
            }
        } else if (myGroupIsActive) {
            switch (position) {
                case 0:
                    return statisticsMyGroupFragment;
                default:
                    return null;
            }
        } else {
            switch (position) {
                case 0:
                    return statisticsPartnerGroupFragment;
                default:
                    return null;
            }
        }
    }

    @Override
    public int getCount() {
        return tabsNumber;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

}
