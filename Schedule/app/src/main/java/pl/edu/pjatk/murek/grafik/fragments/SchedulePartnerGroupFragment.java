package pl.edu.pjatk.murek.grafik.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.adapters.ScheduleListAdapter;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.ScheduleData;
import pl.edu.pjatk.murek.grafik.models.SourceGroup;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-29.
 */

@EFragment(R.layout.schedule_layout)
public class SchedulePartnerGroupFragment extends Fragment implements Callback<List<ScheduleData>> {
    @ViewById(R.id.schedule_ListView)
    protected ListView listView;

    private FragmentManager fragmentManager;

    @ViewById(R.id.progress_view_schedule)
    protected CircularProgressView progressView;

    @ViewById(R.id.swipe_refresh_layout_schedule)
    protected SwipeRefreshLayout swipeRefreshLayout;

    private static Context context;

    public static void setContext(Context context) {
        SchedulePartnerGroupFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.schedule_layout, container, false);
    }

    @AfterViews
    protected void afterViews() {
        ((DrawerActivity) context).getToolbar().setTitle(R.string.schedule);
        fragmentManager = ((DrawerActivity) context).getSupportFragmentManager();
        swipeRefreshLayout.setColorSchemeResources(R.color.colorMenuUp);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getScheduleList();
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listView.getChildAt(0) != null) {
                    swipeRefreshLayout.setEnabled(listView.getFirstVisiblePosition() == 0 && listView.getChildAt(0).getTop() == 0);
                }
            }
        });

        getScheduleList();
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void getScheduleList() {
        progressView.startAnimation();

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<List<ScheduleData>> call = apiService.getSchedule(userSession.getIdPartnerGroup(), userSession.getIdAccount());
        call.enqueue(this);
    }

    @Override
    public void onResponse(Response<List<ScheduleData>> response, Retrofit retrofit) {
        disableRefreshEffect();

        if (response.body() != null) {
            ScheduleListAdapter adapter = new ScheduleListAdapter(context, response.body(), fragmentManager, listView, SourceGroup.PARTNER_GROUP);
            listView.setAdapter(adapter);
        } else {
            Toast.makeText(context, R.string.toast_get_schedule_fail, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        disableRefreshEffect();

        Toast.makeText(context, R.string.toast_server_fail, Toast.LENGTH_SHORT).show();
    }
}