package pl.edu.pjatk.murek.grafik.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.squareup.okhttp.ResponseBody;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.ViewById;

import java.util.Calendar;
import java.util.Date;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.EditGroupData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.DateUtils;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-19.
 */
@Setter
@EFragment(R.layout.my_group_settings_layout)
public class MyGroupSettingsFragment extends Fragment implements Callback<ResponseBody> {

    private int year, month, day;
    private java.util.Calendar calendar;
    private DatePickerDialog dpd;
    private boolean ifStartedGroup;
    @ViewById(R.id.et_group_name)
    protected EditText groupNameET;
    @ViewById(R.id.et_password)
    protected EditText passwordET;
    @ViewById(R.id.et_first_cleaning_date)
    protected EditText firstCleaningDateET;
    @ViewById(R.id.et_how_many_days)
    protected EditText howManyDaysET;
    @ViewById(R.id.et_percentage_return)
    protected EditText percentageReturnET;
    @ViewById(R.id.save_group_settings)
    protected Button saveGroupSettingsBTN;
    @ViewById(R.id.progress_view_group_settings)
    protected CircularProgressView progressView;
    @ViewById(R.id.if_group_is_active)
    protected CheckBox ifGroupIsActiveCheckBox;

    private static Context context;

    public static void setContext(Context context) {
        MyGroupSettingsFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_group_settings_layout, container, false);
    }

    @AfterViews
    protected void afterViews() {
        ((DrawerActivity) context).getToolbar().setTitle(R.string.my_group_settings);
        sendRequestGetGroupData();
    }

    @Click(R.id.save_group_settings)
    protected void changeGroupSettingsClick() {
        String groupName = groupNameET.getText().toString();
        String password = passwordET.getText().toString();
        String cleaningDate = firstCleaningDateET.getText().toString();
        String howManyDays = this.howManyDaysET.getText().toString();
        String percentageReturn = percentageReturnET.getText().toString();


        if (ifCorrectData(groupName, password, cleaningDate, howManyDays, percentageReturn)) {
            changeGroupSettings(groupName, password, cleaningDate, Integer.parseInt(howManyDays), Integer.parseInt(percentageReturn));
        }
    }

    private void changeGroupSettings(String groupName, String password, String firstCleaningDate, int howManyDays, int percentageReturn) {

        EditGroupData editGroupData = new EditGroupData(groupName, password, firstCleaningDate, howManyDays, percentageReturn, ifGroupIsActiveCheckBox.isChecked(), ifStartedGroup);
        sendRequest(editGroupData);
    }

    private boolean ifCorrectData(String groupName, String password, String firstCleaningDate, String howManyDays, String percentageReturn) {

        if (validationOnEmpty(groupName, password, firstCleaningDate, howManyDays, percentageReturn)) {
            int percentageReturnInt = Integer.parseInt(percentageReturn);
            int howManyDaysInt = Integer.parseInt(howManyDays);
            Date parsedDate = DateUtils.convertStringToDateTime(firstCleaningDate);
            return businessValidation(parsedDate, howManyDaysInt, percentageReturnInt);
        }

        return false;
    }

    private boolean validationOnEmpty(String groupName, String password, String firstCleaningDate, String howManyDays, String percentageReturn) {
        if (groupName.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_group_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!ifStartedGroup && firstCleaningDate.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_cleaning_date), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (howManyDays.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_how_many_days), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (percentageReturn.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_percentage_return), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean businessValidation(Date firstCleaningDate, int howManyDays, int percentageReturn) {

        if (howManyDays < 1) {
            Toast.makeText(context, getString(R.string.toast_walidation_field_value_how_many_days_must_be_bigger_than_zero), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!ifStartedGroup && firstCleaningDate == null) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_cleaning_date), Toast.LENGTH_SHORT).show();
            return false;
        }


        if (percentageReturn > 100) {
            Toast.makeText(context, getString(R.string.toast_walidation_percentage_return_can_not_be_bigger_than_100), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
    }

    private void sendRequest(EditGroupData editGroupData) {
        progressView.startAnimation();
        progressView.setVisibility(View.VISIBLE);
        saveGroupSettingsBTN.setEnabled(false);
        ifGroupIsActiveCheckBox.setEnabled(false);

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<ResponseBody> call = apiService.editGroup(editGroupData, userSession.getIdMyGroup(), userSession.getIdAccount());
        call.enqueue(this);
    }

    @FocusChange(R.id.et_first_cleaning_date)
    void focusOnFirstCleaningDate(View firstCleaningData, boolean hasFocus) {
        if (hasFocus && !ifStartedGroup) {

            calendar = Calendar.getInstance();
            day = calendar.get(Calendar.DAY_OF_MONTH);
            month = calendar.get(Calendar.MONTH);
            year = calendar.get(Calendar.YEAR);

            if (!((Activity) context).isFinishing()) {
                dpd = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        firstCleaningDateET.setText(DateUtils.convertToDateWithZero(year, month + 1, dayOfMonth));
                        howManyDaysET.requestFocus();
                    }
                }, year, month, day);
                dpd.show();
            }
        }

    }

    @Override
    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
        disableRefreshEffect();
        saveGroupSettingsBTN.setEnabled(true);
        boolean valueFromCheckBox = ifGroupIsActiveCheckBox.isChecked();
        ifGroupIsActiveCheckBox.setEnabled(true);

        if (response.code() == 200) {
            Toast.makeText(context, getString(R.string.toast_edit_group_success), Toast.LENGTH_LONG).show();
            setGroupActivity(valueFromCheckBox);
        } else {
            ResponseBody responseBody = response.errorBody();
            MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.toast_edit_group_fail));
        }
    }

    @Override
    public void onFailure(Throwable t) {
        disableRefreshEffect();
        saveGroupSettingsBTN.setEnabled(true);
        ifGroupIsActiveCheckBox.setEnabled(true);

        Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }

    public void sendRequestGetGroupData() {
        progressView.startAnimation();
        progressView.setVisibility(View.VISIBLE);

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<EditGroupData> call = apiService.getGroupData(userSession.getIdMyGroup(), userSession.getIdAccount());

        call.enqueue(new Callback<EditGroupData>() {
            @Override
            public void onResponse(Response<EditGroupData> response, Retrofit retrofit) {
                disableRefreshEffect();

                if (response.code() == 200) {
                    Toast.makeText(context, getString(R.string.toast_get_group_settings_success), Toast.LENGTH_LONG).show();
                    EditGroupData editGroupData = response.body();
                    if(editGroupData != null) {
                        groupNameET.setText(editGroupData.getGroupName());
                        passwordET.setText(editGroupData.getGroupPassword());

                        String dateFromDatebaseString = editGroupData.getFirstCleaningData().split(" ")[0];
                        Date dateFromDatabaseDate = DateUtils.convertStringToDateTime(dateFromDatebaseString);
                        firstCleaningDateET.setText(DateUtils.formatDate(dateFromDatabaseDate));


                        howManyDaysET.setText("" + editGroupData.getHowManyDays());
                        percentageReturnET.setText("" + editGroupData.getPercentageReturn());
                        ifGroupIsActiveCheckBox.setChecked(editGroupData.isIfActive());
                        ifStartedGroup = editGroupData.isIfStarted();
                        firstCleaningDateET.setEnabled(!ifStartedGroup);
                    }

                } else {
                    ResponseBody responseBody = response.errorBody();
                    MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.toast_get_group_settings_fail));
                }

            }

            @Override
            public void onFailure(Throwable t) {
                disableRefreshEffect();

                Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void setGroupActivity(boolean activity) {
        Session session = MySharedPreferencesUtils.getSession((Activity) context);
        if (activity != session.isMyGroupIsActive()) {
            session.setMyGroupIsActive(activity);
            MySharedPreferencesUtils.saveSession(session, (Activity) context);
        }
    }
}
