package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-21.
 */
@Getter
@Setter
public class MemberData {

    @SerializedName("IdKonta")
    private int idAccount;

    @SerializedName("Login")
    private String login;

    @SerializedName("Zdjecie")
    private String photo;

    public MemberData(int idAccount, String login, String photo) {
        this.idAccount = idAccount;
        this.login = login;
        this.photo = photo;
    }
}
