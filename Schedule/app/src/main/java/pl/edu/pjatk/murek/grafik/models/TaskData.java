package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */
@Getter
@Setter
public class TaskData {

    @SerializedName("IdZadania")
    private int idTask;

    @SerializedName("Nazwa")
    private String name;

    @SerializedName("Opis")
    private String description;

    @SerializedName("Zdjecie")
    private String photo;

    @SerializedName("NazwaIkony")
    private String iconName;

    @SerializedName("Nagranie")
    private String recording;

    @SerializedName("Punkty")
    private int points;

    public TaskData(int idTask, String name, String description, String photo, String iconName, String recording, int points) {
        this.idTask = idTask;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.iconName = iconName;
        this.recording = recording;
        this.points = points;
    }
}
