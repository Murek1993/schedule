package pl.edu.pjatk.murek.grafik.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.squareup.okhttp.ResponseBody;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.MainActivity_;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.RegistrationData;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-29.
 */

@EFragment(R.layout.registration_layout)
public class RegistrationFragment extends Fragment implements Callback<ResponseBody> {

    @ViewById(R.id.login_registration_editText)
    protected EditText loginRegistrationET;

    @ViewById(R.id.password_registation_editText)
    protected EditText passwordRegistrationET;

    @ViewById(R.id.repeat_password_registration_editText)
    protected EditText repeatPasswordRegistrationET;

    @ViewById(R.id.register_button)
    protected Button registerButton;

    @ViewById(R.id.progress_view_registration)
    protected CircularProgressView progressView;


    private static Context context;

    public static void setContext(Context context) {
        RegistrationFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.registration_layout, container, false);
    }


    @Click(R.id.register_button)
    protected void registerClick() {
        String login = loginRegistrationET.getText().toString();
        String password = passwordRegistrationET.getText().toString();
        String repeatPassword = repeatPasswordRegistrationET.getText().toString();
        if (ifCorrectData(login, password, repeatPassword)) {
            RegistrationData registrationData = new RegistrationData(login.trim(), password, repeatPassword);
            sendRequest(registrationData);
        }
    }

    private boolean ifCorrectData(String login, String password, String repeatPassword) {
        if (login.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_login), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_log_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (repeatPassword.equals("")) {
            Toast.makeText(context, getString(R.string.toast_fill_repeat_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (!password.equals(repeatPassword)) {
            Toast.makeText(context, getString(R.string.toast_mistakenly_repeated_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void sendRequest(RegistrationData registrationData) {
        progressView.startAnimation();
        progressView.setVisibility(View.VISIBLE);
        registerButton.setEnabled(false);

        ApiService apiService = new ApiController(context).getAPIServiceWithOutAuthorization();
        Call<ResponseBody> call = apiService.createAccount(registrationData);
        call.enqueue(this);
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
        disableRefreshEffect();
        registerButton.setEnabled(true);

        if (response.code() == 200) {
            loginRegistrationET.getText().clear();
            passwordRegistrationET.getText().clear();
            repeatPasswordRegistrationET.getText().clear();
            MainActivity_ parentActivity = (MainActivity_) context;
            parentActivity.getViewPager().setCurrentItem(0);
            Toast.makeText(context, R.string.toast_create_account_success, Toast.LENGTH_SHORT).show();
        } else {
            ResponseBody responseBody = response.errorBody();
            MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.toast_create_account_fail));

        }
    }

    @Override
    public void onFailure(Throwable t) {
        disableRefreshEffect();
        registerButton.setEnabled(true);

        Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }
}
