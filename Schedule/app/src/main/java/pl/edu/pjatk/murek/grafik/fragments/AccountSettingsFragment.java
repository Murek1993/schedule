package pl.edu.pjatk.murek.grafik.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.squareup.okhttp.ResponseBody;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.SettingsAccountData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import pl.edu.pjatk.murek.grafik.utils.DownloadedPhotosUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-19.
 */
@Setter
@EFragment(R.layout.account_setting_layout)
public class AccountSettingsFragment extends Fragment implements Callback<ResponseBody> {

    private static final int CODE_PHOTO_FROM_CAMERA = 1;
    private static final int CODE_PHOTO_FROM_GALLERY = 2;

    @ViewById(R.id.et_photo_account)
    protected EditText photoET;
    @ViewById(R.id.et_old_password_account)
    protected EditText oldPasswordET;
    @ViewById(R.id.et_new_password_account)
    protected EditText newPasswordET;
    @ViewById(R.id.et_repeat_password_account)
    protected EditText repeatPasswordET;
    @ViewById(R.id.btn_save_settings_account)
    protected Button saveSettingsAccountBTN;
    @ViewById(R.id.progress_view_settings_account)
    protected CircularProgressView progressView;

    private String photoPath = "";
    String photo = null;


    private static Context context;

    public static void setContext(Context context) {
        AccountSettingsFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.account_setting_layout, container, false);
    }

    @AfterViews
    protected void afterViews() {
        ((DrawerActivity) context).getToolbar().setTitle(R.string.settings_account);
    }

    @FocusChange(R.id.et_photo_account)
    void focusNaZdjecie(View photo, boolean hasFocus) {
        if (hasFocus) {
            final CharSequence[] elements = {getString(R.string.take_photo), getString(R.string.select_from_gallery), getString(R.string.cancel)};
            if (!((Activity) context).isFinishing()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                LayoutInflater inflater = ((DrawerActivity) context).getLayoutInflater();

                View heading = inflater.inflate(R.layout.menu_photo_title_layout, null);
                builder.setCustomTitle(heading);
                builder.setItems(elements, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (elements[item].equals(elements[0])) {
                            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, CODE_PHOTO_FROM_CAMERA);
                            } else {
                                takePhoto();
                            }
                        } else if (elements[item].equals(elements[1])) {
                            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_PHOTO_FROM_GALLERY);
                            } else {
                                getPhotoFromGallery();
                            }
                        } else if (elements[item].equals(elements[2])) {
                            dialog.dismiss();
                        }
                    }
                });

                AlertDialog alertDialog = builder.create();
                ListView listView = alertDialog.getListView();
                listView.setDivider(new ColorDrawable(Color.BLACK));
                listView.setDividerHeight(1);
                listView.setOverscrollFooter(new ColorDrawable(Color.TRANSPARENT));
                listView.setHeaderDividersEnabled(true);

                alertDialog.show();
            }
        }
    }

    private void takePhoto(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CODE_PHOTO_FROM_CAMERA);
    }

    private void getPhotoFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, CODE_PHOTO_FROM_GALLERY);
    }

    @Click(R.id.btn_save_settings_account)
    protected void saveSettingsAccountClick() {
        String oldPassword = oldPasswordET.getText().toString();
        String newPassword = newPasswordET.getText().toString();
        String repeatPassword = repeatPasswordET.getText().toString();
        if (ifCorrectData(oldPassword, newPassword, repeatPassword)) {
            saveSettings(oldPassword, newPassword, repeatPassword);
        }

    }

    private void saveSettings(String oldPassword, String newPassword, String repeatPassword) {

        String photoName = photoET.getText().toString();

        if (!photoName.equals("") && !photoPath.equals("")) {
            photo = downloadPhotoData(photoPath);
        }

        SettingsAccountData settingsAccountData = new SettingsAccountData(oldPassword, newPassword, repeatPassword, photo);
        sendRequest(settingsAccountData);
    }


    private String downloadPhotoData(String photoPath) {

        Bitmap bitmap = DownloadedPhotosUtils.getThumbnailBitmap(photoPath, 160);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] photoByte = stream.toByteArray();


        return Base64.encodeToString(photoByte, Base64.DEFAULT);
    }


    private boolean ifCorrectData(String oldPassword, String newPassword, String repeatPassword) {
        if (ifEmptyForm(oldPassword, newPassword, repeatPassword) == false) {
            return false;
        }
        if (oldPassword.equals("") && (!newPassword.equals("") || !repeatPassword.equals(""))) {
            Toast.makeText(context, getString(R.string.toast_fill_old_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        if ((!newPassword.equals("") || !repeatPassword.equals("")) && !newPassword.equals(repeatPassword)) {
            Toast.makeText(context, getString(R.string.toast_mistakenly_repeated_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean ifEmptyForm(String oldPassword, String newPassword, String repeatPassword) {
        String photoName = photoET.getText().toString();

        if (oldPassword.equals("") && newPassword.equals("") && repeatPassword.equals("") && photoName.equals("")) {
            return false;
        }
        return true;
    }

    private void sendRequest(SettingsAccountData settingsAccountData) {
        progressView.startAnimation();
        progressView.setVisibility(View.VISIBLE);
        saveSettingsAccountBTN.setEnabled(false);

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<ResponseBody> call = apiService.saveSettingsAccount(settingsAccountData, userSession.getIdAccount());
        call.enqueue(this);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (data != null && resultCode == Activity.RESULT_OK) {
                if (requestCode == CODE_PHOTO_FROM_CAMERA) {
                    photoPath = DownloadedPhotosUtils.takePhoto(data);
                    String[] fileName = photoPath.split("/");
                    photoET.setText(fileName[fileName.length - 1]);
                    oldPasswordET.requestFocus();
                } else if (requestCode == CODE_PHOTO_FROM_GALLERY) {
                    photoPath = DownloadedPhotosUtils.takePhotoFromGallery(data, ((DrawerActivity) context));
                    String[] fileName = photoPath.split("/");
                    photoET.setText(fileName[fileName.length - 1]);
                    oldPasswordET.requestFocus();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
        disableRefreshEffect();
        saveSettingsAccountBTN.setEnabled(true);

        if (response.code() == 200) {

            Session usserSession = MySharedPreferencesUtils.getSession((Activity) context);
            usserSession.setPhoto(photo);
            MySharedPreferencesUtils.saveSession(usserSession, (Activity) context);
            ((DrawerActivity) context).setLoginAndPhotoFromSession(usserSession);

            Toast.makeText(context, getString(R.string.toast_save_account_settings_success), Toast.LENGTH_SHORT).show();
            clearForm();
        } else {
            ResponseBody responseBody = response.errorBody();
            MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.toast_save_account_settings_fail));
        }
    }

    @Override
    public void onFailure(Throwable t) {
        disableRefreshEffect();
        saveSettingsAccountBTN.setEnabled(true);

        Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }

    private void clearForm() {
        oldPasswordET.getText().clear();
        newPasswordET.getText().clear();
        repeatPasswordET.getText().clear();
        photoET.getText().clear();
        photoPath = "";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == CODE_PHOTO_FROM_CAMERA) {
            takePhoto();
        } else if (requestCode == CODE_PHOTO_FROM_GALLERY) {
            getPhotoFromGallery();
        }
    }

}
