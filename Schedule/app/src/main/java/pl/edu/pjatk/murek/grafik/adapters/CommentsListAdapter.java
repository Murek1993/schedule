package pl.edu.pjatk.murek.grafik.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.models.Comment;

/**
 * Created on 2017-04-21.
 */

public class CommentsListAdapter extends ArrayAdapter<Comment> {
    private List<Comment> list;
    private Context context;

    public CommentsListAdapter(Context context, List<Comment> list) {
        super(context, android.R.layout.simple_list_item_1, list);
        this.list = list != null? list : new ArrayList<Comment>();
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {


            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            if (position % 2 == 0) {
                convertView = inflater.inflate(R.layout.comments_list_row_left, parent, false);

            } else {
                convertView = inflater.inflate(R.layout.comments_list_row_right, parent, false);
            }
        }
        TextView login = (TextView) convertView.findViewById(R.id.commentsLogin);
        TextView date = (TextView) convertView.findViewById(R.id.commentsDateOfComment);
        TextView comment = (TextView) convertView.findViewById(R.id.commentsComment);

        login.setText(list.get(position).getLogin());
        String dateOfComment = list.get(position).getDateOfComment();
        if (dateOfComment != null) {
            dateOfComment = dateOfComment.replace('T', ' ');
            dateOfComment = dateOfComment.split("\\.")[0];
            date.setText(dateOfComment);
        }

        comment.setText(list.get(position).getCommentContent());
        CircleImageView photo = (CircleImageView) convertView.findViewById(R.id.comments_photo);
        String downloadedPhoto = list.get(position).getPhoto();
        if (downloadedPhoto != null) {
            byte[] photoByteSize = Base64.decode(downloadedPhoto, Base64.DEFAULT);

            Bitmap bmp = BitmapFactory.decodeByteArray(photoByteSize, 0, photoByteSize.length);
            photo.setImageBitmap(bmp);
        } else {

            photo.setImageResource(R.mipmap.empty_avatar);
        }

        return convertView;
    }
    @Override
    public int getItemViewType(int position) {
        return position % 2 == 0 ? 1 : 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

}
