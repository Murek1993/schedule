package pl.edu.pjatk.murek.grafik.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.adapters.StatisticsAdapter;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;

/**
 * Created on 2017-04-19.
 */
@Setter
@EFragment(R.layout.tab_view)
public class StatisticsFragment extends Fragment {

    @ViewById(R.id.tab_layout)
    protected TabLayout tabLayout;

    @ViewById(R.id.pager)
    protected ViewPager viewPager;

    private FragmentManager fragmentManager;

    private static Context context;

    public static void setContext(Context context) {
        StatisticsFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_view, container, false);
    }

    @AfterViews
    public void afterViews() {
        ((DrawerActivity) context).getToolbar().setTitle(R.string.statistics);
        fragmentManager = ((DrawerActivity) context).getSupportFragmentManager();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        setTabs(userSession);
        setViewPager(userSession.isMyGroupIsActive(), userSession.isPartnerGroupIsActive());
    }

    private void setTabs(Session userSession) {
        boolean ifHaveGroup = userSession != null && userSession.getIdPartnerGroup() != null;

        if (ifHaveGroup && userSession.isMyGroupIsActive() && userSession.isPartnerGroupIsActive()) {
            tabLayout.addTab(tabLayout.newTab().setText(R.string.my_group));
            tabLayout.addTab(tabLayout.newTab().setText(R.string.partner_group));
        } else if (ifHaveGroup && !userSession.isMyGroupIsActive() && userSession.isPartnerGroupIsActive()) {
            tabLayout.addTab(tabLayout.newTab().setText(R.string.partner_group));
        } else {
            tabLayout.addTab(tabLayout.newTab().setText(R.string.my_group));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
    }

    private void setViewPager(boolean myGroupIsActive, boolean partnerGroupIsActive) {
        final StatisticsAdapter adapter = new StatisticsAdapter(fragmentManager, tabLayout.getTabCount(), context, myGroupIsActive, partnerGroupIsActive);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

}
