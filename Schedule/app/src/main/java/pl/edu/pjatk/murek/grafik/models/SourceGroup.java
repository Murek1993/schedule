package pl.edu.pjatk.murek.grafik.models;

/**
 * Created on 2017-05-01.
 */

public enum SourceGroup {
    MY_GROUP, PARTNER_GROUP
}
