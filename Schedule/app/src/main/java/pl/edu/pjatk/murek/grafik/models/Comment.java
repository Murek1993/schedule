package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-22.
 */
@Getter
@Setter
public class Comment {

    @SerializedName("Login")
    private String login;

    @SerializedName("Zdjecie")
    private String photo;

    @SerializedName("TrescKomentarza")
    private String commentContent;

    @SerializedName("DataKomentarza")
    private String dateOfComment;

    public Comment(String login, String photo, String commentContent, String dateOfComment) {
        this.login = login;
        this.photo = photo;
        this.commentContent = commentContent;
        this.dateOfComment = dateOfComment;
    }
}
