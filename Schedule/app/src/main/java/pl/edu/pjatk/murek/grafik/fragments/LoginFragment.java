package pl.edu.pjatk.murek.grafik.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.squareup.okhttp.ResponseBody;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity_;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.LoginData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-29.
 */

@EFragment(R.layout.activity_main)
public class LoginFragment extends Fragment implements Callback<Session> {

    @ViewById(R.id.loginEditText)
    protected EditText loginET;

    @ViewById(R.id.passwordEditText)
    protected EditText passwordET;

    @ViewById(R.id.login_button)
    protected Button loginButton;

    @ViewById(R.id.progress_view_login)
    protected CircularProgressView progressView;

    private static Context context;

    public static void setContext(Context context) {
        LoginFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_main, container, false);
    }

    @Click(R.id.login_button)
    protected void loginClick() {
        String login = loginET.getText().toString();
        String password = passwordET.getText().toString();
        if (idCorrectData(login, password)) {
            LoginData loginData = new LoginData(login, password);
            sendRequest(loginData);
        }
    }

    private boolean idCorrectData(String login, String password) {
        if (login.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_login), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_log_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void sendRequest(LoginData loginData) {
        progressView.startAnimation();
        progressView.setVisibility(View.VISIBLE);
        loginButton.setEnabled(false);

        ApiService apiService = new ApiController(context).getAPIServiceWithOutAuthorization();
        Call<Session> call = apiService.login(loginData);
        call.enqueue(this);
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponse(Response<Session> response, Retrofit retrofit) {
        disableRefreshEffect();
        loginButton.setEnabled(true);

        if (response.body() != null) {
            MySharedPreferencesUtils.saveSession(response.body(), (Activity) context);
            Intent intent = new Intent(context, DrawerActivity_.class);
            startActivity(intent);
            ((Activity) context).finish();
        } else {
            ResponseBody responseBody = response.errorBody();
            MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.toast_login_fail));

        }
    }

    @Override
    public void onFailure(Throwable t) {
        disableRefreshEffect();
        loginButton.setEnabled(true);

        Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }
}
