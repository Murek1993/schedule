package pl.edu.pjatk.murek.grafik.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import com.andexert.expandablelayout.library.ExpandableLayoutListView;
import com.github.rahatarmanahmed.cpv.CircularProgressView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.adapters.TasksRatedListAdapter;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.TaskDataWithStatus;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-19.
 */
@Setter
@EFragment(R.layout.tasks_rated_layout)
public class TasksRatedFragment extends Fragment implements Callback<List<TaskDataWithStatus>> {

    @ViewById(R.id.expandable_list_view_rated)
    protected ExpandableLayoutListView listView;

    @ViewById(R.id.progress_view_tasks_rated)
    protected CircularProgressView progressView;

    @ViewById(R.id.swipe_refresh_layout_tasks_rated)
    protected SwipeRefreshLayout swipeRefreshLayout;

    private boolean ifAction;

    private FragmentManager fragmentManager;

    private static Context context;

    public static void setContext(Context context) {
        TasksRatedFragment.context = context;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tasks_rated_layout, container, false);
    }

    @AfterViews
    protected void afterViews() {
        ((DrawerActivity) context).getToolbar().setTitle(R.string.tasks);
        fragmentManager = ((DrawerActivity) context).getSupportFragmentManager();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorMenuUp);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getTasksRatedList();
            }
        });

        listView.setOnScrollListener((new ExpandableLayoutListView(context)).new OnExpandableLayoutScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (listView.getChildAt(0) != null) {
                    swipeRefreshLayout.setEnabled(listView.getFirstVisiblePosition() == 0 && listView.getChildAt(0).getTop() == 0);
                }
            }
        });

        getTasksRatedList();
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void getTasksRatedList() {
        progressView.startAnimation();

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Bundle arguments = getArguments();
        int idGroup = arguments.getInt("idGroup");
        int idCleaning = arguments.getInt("idCleaning");
        ifAction = arguments.getBoolean("ifAction");
        Call<List<TaskDataWithStatus>> call = apiService.getTaskListWithStatus(idGroup, idCleaning);
        call.enqueue(this);

    }


    @Override
    public void onResponse(Response<List<TaskDataWithStatus>> response, Retrofit retrofit) {
        disableRefreshEffect();

        if (response.body() != null) {
            TasksRatedListAdapter adapter = new TasksRatedListAdapter(context, response.body(), ifAction, fragmentManager);
            listView.setAdapter(adapter);
        } else {
            Toast.makeText(context, R.string.toast_get_tasks_list_fail, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        disableRefreshEffect();

        Toast.makeText(context, R.string.toast_server_fail, Toast.LENGTH_SHORT).show();
    }
}
