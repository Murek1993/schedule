package pl.edu.pjatk.murek.grafik.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.ResponseBody;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.adapters.SendSmsListAdapter;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-19.
 */
@Setter
@EFragment(R.layout.send_sms_layout)
public class SendSmsFragment extends Fragment implements Callback<String> {

    private static final int CODE_CONTACTS = 101;
    private static final int CODE_SMS = 102;
    @ViewById(R.id.sms_ListView)
    public ListView listView;
    @ViewById(R.id.smsInfoListView)
    public TextView info;
    private SendSmsListAdapter adapter;
    private static Context context;
    private List<String> nonCorrectContacts = new ArrayList<>();
    private String message = "";

    public static void setContext(Context context) {
        SendSmsFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.send_sms_layout, container, false);
    }

    @AfterViews
    protected void afterViews() {
        sendRequestGetGroupData();
        ((DrawerActivity) context).getToolbar().setTitle(R.string.send_password_sms);
        adapter = new SendSmsListAdapter(context, new ArrayList<String>());
        listView.setAdapter(adapter);
    }

    @Click(R.id.btn_send_sms)
    protected void sendPasswordSMSClick() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.SEND_SMS}, CODE_SMS);
        } else if (adapter.getContacts().size() == 0) {

            Toast.makeText(context, R.string.toast_sms_empty_list, Toast.LENGTH_LONG).show();
        } else {

            sendSMS();
        }
    }
    private void sendSMS() {
        SmsManager smsManager = SmsManager.getDefault();

        for (Map.Entry<String, String> contact : adapter.getContacts().entrySet()) {
            try {
                smsManager.sendTextMessage(contact.getValue(), null, message, null, null);
                Toast.makeText(context, R.string.toast_smss_sent, Toast.LENGTH_LONG).show();
            } catch (Exception e) {
                Toast.makeText(context, R.string.toast_smss_not_sent_try_again, Toast.LENGTH_LONG).show();
                nonCorrectContacts.add(contact.getKey());
                e.printStackTrace();
            }
        }
        if (!nonCorrectContacts.isEmpty()) {
            String message = getString(R.string.toast_sms_bot_sent_to);
            for (String contact : nonCorrectContacts) {
                message = message + " " + contact;
            }
            info.setText(message);
        }
        clearList();
    }

    private void clearList() {
        adapter.getList().clear();
        adapter.notifyDataSetChanged();
        adapter.getContacts().clear();
    }

    @Click(R.id.btn_add_contact)
    protected void addContact() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.READ_CONTACTS}, CODE_CONTACTS);
        } else {
            addContactFromList();
        }
    }

    private void addContactFromList() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, CODE_CONTACTS);
        info.setText("");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && resultCode == Activity.RESULT_OK) {
            if (requestCode == CODE_CONTACTS) {
                Uri contactData = data.getData();
                Cursor c = ((Activity) context).managedQuery(contactData, null, null, null, null);

                if (c.moveToFirst()) {
                    String id = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));
                    String hasPhone = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                    if (hasPhone.equalsIgnoreCase("1")) {
                        Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                        phones.moveToFirst();

                        String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        String contactName = c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));

                        adapter.getContacts().put(contactName, phoneNumber);
                        Toast.makeText(context, R.string.toast_sms_add_number + phoneNumber, Toast.LENGTH_SHORT).show();
                        adapter.getList().add(contactName);
                        adapter.notifyDataSetChanged();

                    }
                }
            }
        }
    }

    private void sendRequestGetGroupData() {

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<String> call1 = apiService.getContentMessage(userSession.getIdMyGroup(), userSession.getIdAccount());
        call1.enqueue(this);
    }

    @Override
    public void onResponse(Response<String> response, Retrofit retrofit) {

        if (response.code() == 200) {
            message = response.body();

        } else {
            ResponseBody responseBody = response.errorBody();
            MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.toast_get_group_data_fail));
        }

    }

    @Override
    public void onFailure(Throwable t) {
        Toast.makeText(context, R.string.toast_server_fail, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == CODE_CONTACTS) {
            addContactFromList();
        } else if (requestCode == CODE_SMS) {
            sendSMS();
        }
    }

}
