package pl.edu.pjatk.murek.grafik.api;


import com.squareup.okhttp.ResponseBody;

import java.util.List;

import pl.edu.pjatk.murek.grafik.models.JoinToGroupData;
import pl.edu.pjatk.murek.grafik.models.EditGroupData;
import pl.edu.pjatk.murek.grafik.models.ScheduleData;
import pl.edu.pjatk.murek.grafik.models.LoginData;
import pl.edu.pjatk.murek.grafik.models.AfterJoinToGroupData;
import pl.edu.pjatk.murek.grafik.models.RegistrationData;
import pl.edu.pjatk.murek.grafik.models.MemberData;
import pl.edu.pjatk.murek.grafik.models.SettingsAccountData;
import pl.edu.pjatk.murek.grafik.models.TaskData;
import pl.edu.pjatk.murek.grafik.models.TaskToEditData;
import pl.edu.pjatk.murek.grafik.models.TaskDataWithStatus;
import pl.edu.pjatk.murek.grafik.models.Comment;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.models.StatisticsMemberGroup;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created on 2017-04-18.
 */

public interface ApiService {

    /**
     * partner group
     **/
    @POST("api/grupaPartnera/dolaczDoGrupy/{idAccount}")
    Call<AfterJoinToGroupData> joinToGroup(@Body JoinToGroupData joinToGroupData, @Path("idAccount") int idAccount);

    @POST("api/grupaPartnera/opuscGrupe/{idPartnerGroup}/{idAccount}")
    Call<ResponseBody> leaveGroup(@Path("idPartnerGroup") int idPartnerGroup, @Path("idAccount") int idAccount);


    /**
     * account
     **/
    @POST("api/konto/zaloguj")
    Call<Session> login(@Body LoginData loginData);

    @POST("api/konto/stworzKonto")
    Call<ResponseBody> createAccount(@Body RegistrationData registrationData);

    @POST("api/konto/zapiszUstawieniaKonta/{idAccount}")
    Call<ResponseBody> saveSettingsAccount(@Body SettingsAccountData settingsAccountData, @Path("idAccount") int idAccount);


    /**
     * my group
     **/
    @GET("api/mojaGrupa/pobierzUczestnikowGrupy/{idGroup}")
    Call<List<MemberData>> getMembersGroup(@Path("idGroup") int idGroup);

    @DELETE("api/mojaGrupa/usunUczestnika/{idGroup}/{idAccount}/{idAccountToRemove}")
    Call<ResponseBody> removeMember(@Path("idGroup") int idGroup, @Path("idAccount") int idAccount, @Path("idAccountToRemove") int idAccountToRemove);

    @POST("api/mojaGrupa/edytujGrupe/{idGroup}/{idAccount}")
    Call<ResponseBody> editGroup(@Body EditGroupData editGroupData, @Path("idGroup") int idGroup, @Path("idAccount") int idAccount);

    @GET("api/mojaGrupa/pobierzDaneGrupy/{idGroup}/{idAccount}")
    Call<EditGroupData> getGroupData(@Path("idGroup") int idGroup, @Path("idAccount") int idAccount);

    @GET("api/mojaGrupa/pobierzDaneDolaczaniaDoGrupy/{idGroup}/{idAccount}")
    Call<JoinToGroupData> getJoinToGroupData(@Path("idGroup") int idGroup, @Path("idAccount") int idAccount);

    @GET("api/mojaGrupa/pobierzTrescWiadomosci/{idGroup}/{idAccount}")
    Call<String> getContentMessage(@Path("idGroup") int idGroup, @Path("idAccount") int idAccount);

    /**
     * cleaning
     **/
    @GET("api/sprzatanie/pobierzGrafik/{idGroup}/{idAccount}")
    Call<List<ScheduleData>> getSchedule(@Path("idGroup") int idGroup, @Path("idAccount") int idAccount);

    @GET("api/sprzatanie/pobierzKomentarze/{idCleaningTask}")
    Call<List<Comment>> getComments(@Path("idCleaningTask") int idCleaningTask);

    @POST("api/sprzatanie/ocenZadanie/{idCleaningTask}/{rate}")
    Call<ResponseBody> rateTask(@Path("idCleaningTask") int idCleaningTask, @Path("rate") int rate);

    @POST("api/sprzatanie/zatwierdzZadanie/{idCleaningTask}/{status}")
    Call<ResponseBody> confirmTask(@Path("idCleaningTask") int idCleaningTask, @Path("status") int status);

    @POST("api/sprzatanie/dodajKomentarz/{idCleaningTask}/{idAccount}")
    Call<ResponseBody> addComment(@Body String comment, @Path("idCleaningTask") int idCleaningTask, @Path("idAccount") int idAccount);


    /**
     * statistics
     **/
    @GET("api/statystyki/pobierzStatystykiGrupy/{idGroup}")
    Call<List<StatisticsMemberGroup>> getStatisticsGroup(@Path("idGroup") int idGroup);


    /**
     * task
     **/
    @GET("api/zadanie/pobierzListeZadan/{idGroup}")
    Call<List<TaskData>> getTaskList(@Path("idGroup") int idGroup); //Only task without cleaning context

    @GET("api/zadanie/pobierzListeZadanZeStatusem/{idGroup}/{idCleaning}")
    Call<List<TaskDataWithStatus>> getTaskListWithStatus(@Path("idGroup") int idGroup, @Path("idCleaning") int idCleaning);

    @POST("api/zadanie/dodajZadanie/{idGroup}")
    Call<ResponseBody> addTask(@Body TaskToEditData taskToEditData, @Path("idGroup") int idGroup);

    @DELETE("api/zadanie/usunZadanie/{idGroup}/{idTask}")
    Call<ResponseBody> removeTask(@Path("idGroup") int idGroup, @Path("idTask") int idTask);

    @POST("api/zadanie/edytujZadanie/{idGroup}/{idTask}")
    Call<ResponseBody> editTask(@Body TaskToEditData taskToEditData, @Path("idGroup") int idGroup, @Path("idTask") int idTask);


}
