package pl.edu.pjatk.murek.grafik.api;

import android.app.Activity;
import android.content.Context;
import android.util.Base64;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

/**
 * Created on 2017-04-23.
 */

@Setter
public class ApiController {

   // public static final String BASE_URL = "http://192.168.1.5/GrafikApi/";
//        public static final String BASE_URL = "http://192.168.1.2/GrafikApi/";
    public static final String BASE_URL = "http://grafikapi.azurewebsites.net/";
    private Context context;

    public ApiController(Context context) {
        this.context = context;
    }

    public ApiService getAPIServiceWithAuthorization() {
        OkHttpClient okClient = new OkHttpClient();
        okClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
                String loginData = userSession.getIdAccount() + userSession.getLogin();
                String loginDataBase64 = Base64.encodeToString(loginData.getBytes(), Base64.NO_WRAP);
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("Authorization", "Bearer " + userSession.getToken())
                        .header("loginData", loginDataBase64);

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okClient)
                .build();
        return retrofit.create(ApiService.class);
    }


    public ApiService getAPIServiceWithOutAuthorization() {
        OkHttpClient okClient = new OkHttpClient();
        okClient.interceptors().add(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Content-Type", "application/json");

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okClient)
                .build();
        return retrofit.create(ApiService.class);
    }

}
