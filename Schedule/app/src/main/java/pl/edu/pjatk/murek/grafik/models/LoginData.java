package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */
@Getter
@Setter
public class LoginData {

    @SerializedName("Login")
    private String login;

    @SerializedName("Haslo")
    private String password;

    public LoginData(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
