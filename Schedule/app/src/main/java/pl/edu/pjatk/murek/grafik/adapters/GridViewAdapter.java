package pl.edu.pjatk.murek.grafik.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.models.GridViewElementModel;

/**
 * Created on 2017-04-26.
 */

public class GridViewAdapter extends ArrayAdapter {
    private Context context;
    private int layoutResourceId;
    private ArrayList icons = new ArrayList();

    public GridViewAdapter(Context context, int layoutResourceId, ArrayList data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.icons = data;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        GridViewHolder holder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new GridViewHolder();
            holder.icon_title = (TextView) row.findViewById(R.id.tv_icon_name);
            holder.icon = (ImageView) row.findViewById(R.id.iv_icon);
            row.setTag(holder);
        } else {
            holder = (GridViewHolder) row.getTag();
        }

        GridViewElementModel element = (GridViewElementModel) icons.get(position);
        holder.icon_title.setText(element.getTitle());
        holder.icon.setImageBitmap(element.getIcon());
        return row;
    }

    private static class GridViewHolder {
        TextView icon_title;
        ImageView icon;
    }
}