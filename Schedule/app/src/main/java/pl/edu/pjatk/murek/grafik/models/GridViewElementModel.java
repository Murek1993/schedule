package pl.edu.pjatk.murek.grafik.models;

import android.graphics.Bitmap;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-26.
 */

@Getter
@Setter
public class GridViewElementModel {
    private Bitmap icon;
    private String title;

    public GridViewElementModel(Bitmap icon, String title) {
        this.icon = icon;
        this.title = title;
    }
}
