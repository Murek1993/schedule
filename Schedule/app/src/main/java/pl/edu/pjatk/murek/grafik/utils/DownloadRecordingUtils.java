package pl.edu.pjatk.murek.grafik.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created on 2017-04-26.
 */

public class DownloadRecordingUtils {

    public static String getRecording(Intent data, Activity activity) {
        Uri savedRecordingUri = data.getData();

        String[] filePathColumn = {MediaStore.Audio.Media.DATA};

        Cursor cursor = activity.getContentResolver().query(savedRecordingUri,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String recordingPath = cursor.getString(columnIndex);
        cursor.close();
        return recordingPath;
    }

    public static void playSound(String recording, Context context) {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            File tempMp3 = File.createTempFile("fmtemp", "mp3", context.getCacheDir());
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            byte[] recordingByte = Base64.decode(recording, Base64.DEFAULT);
            fos.write(recordingByte);
            fos.close();
            mediaPlayer.reset();
            FileInputStream fiss = new FileInputStream(tempMp3);
            mediaPlayer.setDataSource(fiss.getFD());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
