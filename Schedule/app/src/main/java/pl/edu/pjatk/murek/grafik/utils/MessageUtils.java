package pl.edu.pjatk.murek.grafik.utils;

import android.content.Context;
import android.widget.Toast;

import com.squareup.okhttp.ResponseBody;

import org.json.JSONObject;

/**
 * Created on 2017-04-30.
 */

public class MessageUtils {
    public static void showErrorMessagge(Context context, ResponseBody responseBody, String defaultMessage) {
        try {
            JSONObject json = new JSONObject(new String(responseBody.bytes()));
            String ErrorMessageFromApi = json.getString("Message");
            String[] messageFragments = ErrorMessageFromApi.split("API:");
            if (messageFragments.length == 2) {
                Toast.makeText(context, messageFragments[1], Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, defaultMessage, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
