package pl.edu.pjatk.murek.grafik.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.models.StatisticsMemberGroup;

/**
 * Created on 2017-04-21.
 */

public class StatisticsListAdapter extends ArrayAdapter<StatisticsMemberGroup> {
    private List<StatisticsMemberGroup> list;
    private Context context;

    public StatisticsListAdapter(Context context, List<StatisticsMemberGroup> list) {
        super(context, android.R.layout.simple_list_item_1, list);
        this.list = list != null? list : new ArrayList<StatisticsMemberGroup>();
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.statistics_list_row, parent, false);
        }
        TextView login = (TextView) convertView.findViewById(R.id.statisticsLogin);
        TextView cleanup = (TextView) convertView.findViewById(R.id.statisticsCleanup);
        TextView noCleanup = (TextView) convertView.findViewById(R.id.statisticsNoCleanup);
        TextView points = (TextView) convertView.findViewById(R.id.statisticsPoints);

        login.setText(list.get(position).getLogin());
        cleanup.setText("" + list.get(position).getCleanup());
        noCleanup.setText("" + list.get(position).getNoCleanup());
        points.setText("$" + list.get(position).getPoints());

        String downloadedPhoto = list.get(position).getPhoto();
        setPhoto(downloadedPhoto, convertView);

        return convertView;
    }

    public void setPhoto(String downloadedPhoto, View convertView) {
        CircleImageView photo = (CircleImageView) convertView.findViewById(R.id.statisticsPhoto);

        if (downloadedPhoto != null) {
            byte[] photoByteSize = Base64.decode(downloadedPhoto, Base64.DEFAULT);

            Bitmap bmp = BitmapFactory.decodeByteArray(photoByteSize, 0, photoByteSize.length);
            photo.setImageBitmap(bmp);
        } else {

            photo.setImageResource(R.mipmap.empty_avatar);
        }
    }

}
