package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */
@Getter
@Setter
public class JoinToGroupData implements Serializable{

    @SerializedName("NazwaGrupy")
    private String groupName;

    @SerializedName("HasloGrupy")
    private String groupPassword;

    public JoinToGroupData(String groupName, String groupPassword) {
        this.groupName = groupName;
        this.groupPassword = groupPassword;
    }
}
