package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */
@Getter
@Setter
public class ScheduleData {

    @SerializedName("Login")
    private String login;

    @SerializedName("Zdjecie")
    private String photo;

    @SerializedName("Status")
    private int status;

    @SerializedName("DataSprzatania")
    private String cleaningDate;

    @SerializedName("IdSprzatania")
    private Integer idCleaning;

    @SerializedName("Komunikat")
    private String message;


    public ScheduleData(String login, String photo, int status, String cleaningDate, Integer idCleaning, String message) {
        this.login = login;
        this.photo = photo;
        this.status = status;
        this.cleaningDate = cleaningDate;
        this.idCleaning = idCleaning;
        this.message = message;
    }
}
