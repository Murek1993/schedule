package pl.edu.pjatk.murek.grafik.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import pl.edu.pjatk.murek.grafik.fragments.LoginFragment;
import pl.edu.pjatk.murek.grafik.fragments.LoginFragment_;
import pl.edu.pjatk.murek.grafik.fragments.RegistrationFragment;
import pl.edu.pjatk.murek.grafik.fragments.RegistrationFragment_;


/**
 * Created on 2017-04-29.
 */

public class LoginAdapter extends FragmentStatePagerAdapter {
    private int tabsNumber;

    private static Context context;

    private LoginFragment_ loginFragment;
    private RegistrationFragment_ registrationFragment;

    public LoginAdapter(FragmentManager fm, int tabsNumber, Context c) {
        super(fm);
        this.tabsNumber = tabsNumber;
        context = c;
        loginFragment = new LoginFragment_();
        LoginFragment.setContext(context);
        registrationFragment = new RegistrationFragment_();
        RegistrationFragment.setContext(context);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return loginFragment;
            case 1:
                return registrationFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabsNumber;
    }
}
