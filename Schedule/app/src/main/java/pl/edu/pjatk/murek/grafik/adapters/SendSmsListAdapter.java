package pl.edu.pjatk.murek.grafik.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;

/**
 * Created on 2017-04-21.
 */

@Getter
@Setter
public class SendSmsListAdapter extends ArrayAdapter<String> {
    public ImageButton btnRemove;
    private List<String> list;
    private Context context;
    private HashMap<String, String> contacts;

    public SendSmsListAdapter(Context context, List<String> list) {
        super(context, android.R.layout.simple_list_item_1, list);
        this.list = list;
        this.context = context;
        contacts = new HashMap<>();
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.send_sms_list_row, parent, false);
        }

        btnRemove = (ImageButton) convertView.findViewById(R.id.btn_remove_contact);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contacts.remove(list.get(position));
                list.remove(position);
                notifyDataSetChanged();
            }
        });
        TextView name = (TextView) convertView.findViewById(R.id.smsContactName);
        name.setText(list.get(position));


        return convertView;
    }

}
