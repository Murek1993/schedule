package pl.edu.pjatk.murek.grafik.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.adapters.GridViewAdapter;
import pl.edu.pjatk.murek.grafik.models.GridViewElementModel;

/**
 * Created on 2017-04-26.
 */

@Setter
@EFragment(R.layout.gridview_layout)
public class GridViewChoiceIconFragment extends Fragment {

    @ViewById(R.id.grid_view)
    protected GridView gridView;

    private FragmentManager fragmentManager;

    private static Context context;

    public static void setContext(Context context) {
        GridViewChoiceIconFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.gridview_layout, container, false);
    }

    @AfterViews
    protected void afterViews() {
        ((DrawerActivity) context).getToolbar().setTitle(R.string.create_task);
        fragmentManager = getFragmentManager();
        GridViewAdapter gridViewAdapter = new GridViewAdapter(context, R.layout.gridview_item, getIcons());
        gridView.setAdapter(gridViewAdapter);
    }

    private ArrayList<GridViewElementModel> getIcons() {
        final ArrayList<GridViewElementModel> imageItems = new ArrayList<>();
        for (int i = 0; i < 17; i++) {
            int idIcon = context.getResources().getIdentifier("icon" + (i + 1), "mipmap", context.getPackageName());
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), idIcon);
            imageItems.add(new GridViewElementModel(bitmap, "icon" + (i + 1)));
        }
        return imageItems;
    }

    @ItemClick(R.id.grid_view)
    protected void selectIcon(GridViewElementModel element) {
        String iconTitle = element.getTitle();
        Intent intent = new Intent();
        intent.putExtra("iconTitle", iconTitle);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        fragmentManager.popBackStack();
    }
}
