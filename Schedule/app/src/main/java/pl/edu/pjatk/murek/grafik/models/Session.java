package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */
@Getter
@Setter
public class Session {

    @SerializedName("KontoId")
    private int idAccount;

    @SerializedName("Login")
    private String login;

    @SerializedName("Zdjecie")
    private String photo;

    @SerializedName("IdMojejGrupy")
    private int idMyGroup;

    @SerializedName("IdGrupyPartnera")
    private Integer idPartnerGroup;

    @SerializedName("Token")
    private String token;

    @SerializedName("MojaGrupaJestAktywna")
    private boolean myGroupIsActive;

    @SerializedName("GrupaPartneraJestAktywna")
    private boolean partnerGroupIsActive;

    public Session(int idAccount, String login, String photo, int idMyGroup, Integer idPartnerGroup, String token, boolean myGroupIsActive, boolean partnerGroupIsActive) {
        this.idAccount = idAccount;
        this.login = login;
        this.photo = photo;
        this.idMyGroup = idMyGroup;
        this.idPartnerGroup = idPartnerGroup;
        this.token = token;
        this.myGroupIsActive = myGroupIsActive;
        this.partnerGroupIsActive = partnerGroupIsActive;
    }
}
