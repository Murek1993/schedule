package pl.edu.pjatk.murek.grafik.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import pl.edu.pjatk.murek.grafik.models.Session;

/**
 * Created on 2017-04-21.
 */

public class MySharedPreferencesUtils {

    public static void saveSession(Session session, Activity activity) {
        SharedPreferences sp = activity.getSharedPreferences("login", 0);
        SharedPreferences.Editor editor = sp.edit();
        String sessionToSave = convertSessionToString(session);
        editor.putString("session", sessionToSave);
        editor.commit();
    }

    private static String convertSessionToString(Session session) {
        return new Gson().toJson(session);
    }

    private static Session convertStringToSession(String sessionString) {
        return new Gson().fromJson(sessionString, Session.class);
    }

    public static boolean isLogged(Activity activity) {
        Session savedSession = getSession(activity);
        return savedSession != null;
    }


    public static Session getSession(Activity activity) {
        SharedPreferences sp = activity.getSharedPreferences("login", 0);
        if (sp != null) {
            String sessionString = sp.getString("session", "0");
            return sessionString.equals("0") ? null : convertStringToSession(sessionString);
        }
        return null;
    }

    public static void clearSession(Context context) {
        SharedPreferences settings = context.getSharedPreferences("login", Context.MODE_PRIVATE);
        settings.edit().remove("session").commit();
    }

}
