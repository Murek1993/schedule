package pl.edu.pjatk.murek.grafik.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity_;
import pl.edu.pjatk.murek.grafik.fragments.TasksPerformedFragment_;
import pl.edu.pjatk.murek.grafik.fragments.TasksRatedFragment_;
import pl.edu.pjatk.murek.grafik.models.ScheduleData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.models.SourceGroup;
import pl.edu.pjatk.murek.grafik.models.Statuses;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;

/**
 * Created on 2017-04-21.
 */

public class ScheduleListAdapter extends ArrayAdapter<ScheduleData> implements AdapterView.OnItemClickListener {
    private List<ScheduleData> list;
    private Context context;
    private FragmentManager fragmentManager;
    private ListView listView;
    private SourceGroup sourceGroup;

    public ScheduleListAdapter(Context context, List<ScheduleData> list, FragmentManager fragmentManager, ListView listView, SourceGroup sourceGroup) {
        super(context, android.R.layout.simple_list_item_1, list);
        this.list = list != null? list : new ArrayList<ScheduleData>();
        this.context = context;
        this.fragmentManager = fragmentManager;
        this.listView = listView;
        this.sourceGroup = sourceGroup;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.schedule_list_row, parent, false);
        }
        TextView login = (TextView) convertView.findViewById(R.id.scheduleLogin);
        TextView timeMessage = (TextView) convertView.findViewById(R.id.scheduleTimeMessage);
        TextView date = (TextView) convertView.findViewById(R.id.scheduleDateCleaning);
        TextView status = (TextView) convertView.findViewById(R.id.scheduleStatus);
        login.setText(list.get(position).getLogin());
        timeMessage.setText(list.get(position).getMessage());
        date.setText(list.get(position).getCleaningDate().split("T")[0]);
        Statuses stateFromDataBase = Statuses.values()[list.get(position).getStatus()];
        status.setText(stateFromDataBase.toString());

        if (stateFromDataBase.equals(Statuses.REJECTED)) {
            status.setTextColor(Color.RED);
        } else {
            status.setTextColor(context.getResources().getColor(R.color.colorLightGrey));
        }

        CircleImageView photo = (CircleImageView) convertView.findViewById(R.id.schedule_photo);
        String downloadedPhoto = list.get(position).getPhoto();
        if (downloadedPhoto != null) {
            byte[] photoByteSize = Base64.decode(downloadedPhoto, Base64.DEFAULT);

            Bitmap bmp = BitmapFactory.decodeByteArray(photoByteSize, 0, photoByteSize.length);
            photo.setImageBitmap(bmp);
        } else {

            photo.setImageResource(R.mipmap.empty_avatar);
        }
        listView.setOnItemClickListener(this);
        return convertView;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ScheduleData scheduleData = list.get(position);
        Session session = MySharedPreferencesUtils.getSession((Activity) context);
        Bundle arguments = getArgumentsToTransfer(session, scheduleData);
        if (scheduleData.getLogin().equals(session.getLogin())) {
            myCleaning(arguments);
        } else {
            partnerCleaning(arguments);
        }
    }

    private void myCleaning(Bundle arguments) {
        TasksPerformedFragment_ tasksPerformedFragment = new TasksPerformedFragment_();
        TasksPerformedFragment_.setContext(context);
        tasksPerformedFragment.setArguments(arguments);

        fragmentManager.beginTransaction()
                .replace(R.id.container_drawer, tasksPerformedFragment).addToBackStack(null)
                .commit();
        DrawerActivity_.setCurrentFragment(tasksPerformedFragment);
    }


    private void partnerCleaning(Bundle arguments) {
        TasksRatedFragment_ tasksRatedFragment = new TasksRatedFragment_();
        TasksRatedFragment_.setContext(context);
        tasksRatedFragment.setArguments(arguments);

        fragmentManager.beginTransaction()
                .replace(R.id.container_drawer, tasksRatedFragment).addToBackStack(null)
                .commit();
        DrawerActivity_.setCurrentFragment(tasksRatedFragment);
    }

    private Bundle getArgumentsToTransfer(Session session, ScheduleData scheduleData) {
        Bundle arguments = new Bundle();
        if (sourceGroup.equals(SourceGroup.MY_GROUP)) {
            arguments.putInt("idGroup", session.getIdMyGroup());
        } else {
            arguments.putInt("idGroup", session.getIdPartnerGroup());
        }
        int idCleaning = scheduleData.getIdCleaning() == null ? 0 : scheduleData.getIdCleaning();
        arguments.putInt("idCleaning", idCleaning);
        boolean ifAction = scheduleData.getMessage().equals(context.getString(R.string.statement_cleaning_is_in_progress));
        arguments.putBoolean("ifAction", ifAction);
        return arguments;
    }

}
