package pl.edu.pjatk.murek.grafik.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity_;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.fragments.MembersFragment_;
import pl.edu.pjatk.murek.grafik.models.MemberData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-21.
 */

public class MembersListAdapter extends ArrayAdapter<MemberData> implements Callback<ResponseBody> {

    private List<MemberData> list;
    private MemberData memberToRemove;
    private FragmentManager fragmentManager;
    private SweetAlertDialog sweetAlertDialog;

    private Context context;

    public MembersListAdapter(Context context, List<MemberData> list, FragmentManager fragmentManager) {
        super(context, android.R.layout.simple_list_item_1, list);
        this.list = list != null? list: new ArrayList<MemberData>();
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.members_list_row, parent, false);
        }

        String downloadedPhoto = list.get(position).getPhoto();
        setPhoto(downloadedPhoto, convertView);

        TextView login = (TextView) convertView.findViewById(R.id.membersLogin);
        login.setText(list.get(position).getLogin());

        ImageButton btnRemove = (ImageButton) convertView.findViewById(R.id.btn_remove_member);
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                memberToRemove = list.get(position);
                showDialogRemoveMember(memberToRemove.getIdAccount());
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    public void setPhoto(String downloadedPhoto, View convertView) {
        CircleImageView photo = (CircleImageView) convertView.findViewById(R.id.circleView_members);

        if (downloadedPhoto != null) {
            byte[] photoByteSize = Base64.decode(downloadedPhoto, Base64.DEFAULT);

            Bitmap bmp = BitmapFactory.decodeByteArray(photoByteSize, 0, photoByteSize.length);
            photo.setImageBitmap(bmp);
        } else {
            photo.setImageResource(R.mipmap.empty_avatar);
        }
    }

    public void showDialogRemoveMember(final int idMember) {
        if (!((Activity) context).isFinishing()) {
            sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)

                    .setTitleText(context.getString(R.string.dialog_remove))
                    .setContentText(context.getString(R.string.dialog_remove_member_content))
                    .setCancelText(context.getString(R.string.dialog_cancel))
                    .setConfirmText(context.getString(R.string.dialog_confirm))
                    .showCancelButton(true)
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            wyslijRequestUsunUczestnika();

                        }
                    });
            sweetAlertDialog.show();
        }
    }

    private void wyslijRequestUsunUczestnika() {
        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<ResponseBody> call = apiService.removeMember(userSession.getIdMyGroup(),
                userSession.getIdAccount(), memberToRemove.getIdAccount());
        call.enqueue(this);
    }


    @Override
    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
        if (response.code() == 200) {

            MembersFragment_ membersFragment = new MembersFragment_();
            membersFragment.setContext(context);
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, membersFragment)
                    .commit();
            DrawerActivity_.setCurrentFragment(membersFragment);

            sweetAlertDialog
                    .setTitleText(context.getString(R.string.dialog_success))
                    .setContentText(context.getString(R.string.dialog_remove_member_success))
                    .setConfirmText(context.getString(R.string.dialog_ok))
                    .showCancelButton(false)
                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialog.show();

            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.cancel();
                }
            });

        } else {

            ResponseBody responseBody = response.errorBody();
            MessageUtils.showErrorMessagge(context, responseBody, context.getString(R.string.dialog_remove_member_fail));
            sweetAlertDialog
                    .setConfirmClickListener(null)
                    .showCancelButton(false)
                    .setConfirmText(context.getString(R.string.dialog_ok))
                    .setTitleText(context.getString(R.string.dialog_bug))
                    .setContentText(context.getString(R.string.dialog_remove_member_fail));
        }
    }


    @Override
    public void onFailure(Throwable t) {
        Toast.makeText(context, context.getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }

}
