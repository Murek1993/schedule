package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */
@Getter
@Setter
public class RegistrationData {

    @SerializedName("Login")
    private String login;

    @SerializedName("Haslo")
    private String password;

    @SerializedName("PowtorzHaslo")
    private String repeatPassword;

    public RegistrationData(String login, String password, String repeatPassword) {
        this.login = login;
        this.password = password;
        this.repeatPassword = repeatPassword;
    }
}
