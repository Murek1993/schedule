package pl.edu.pjatk.murek.grafik.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.ResponseBody;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity_;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.fragments.TasksFragment_;
import pl.edu.pjatk.murek.grafik.models.TaskData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import pl.edu.pjatk.murek.grafik.utils.DownloadRecordingUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-05-03.
 */

public class TaskListAdapter extends ArrayAdapter<TaskData> implements Callback<ResponseBody> {
    private List<TaskData> list;
    private Context context;
    private FragmentManager fragmentManager;
    private SweetAlertDialog sweetAlertDialog;
    private TaskData taskToRemove;

    public TaskListAdapter(Context context, List<TaskData> list, FragmentManager fragmentManager) {
        super(context, android.R.layout.simple_list_item_1, list);
        this.list = list != null? list : new ArrayList<TaskData>();
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.task_list_list_row, parent, false);
        }

        TextView taskName = (TextView) convertView.findViewById(R.id.tasks_header_text);
        taskName.setText(list.get(position).getName());

        TextView content = (TextView) convertView.findViewById(R.id.task_description);
        content.setText(list.get(position).getDescription());

        ImageButton speaker = (ImageButton) convertView.findViewById(R.id.imageButton_speaker);
        setSpeaker(position, speaker);

        String downloadedPhoto = list.get(position).getPhoto();
        if (downloadedPhoto != null) {
            byte[] photoByteSize = Base64.decode(downloadedPhoto, Base64.DEFAULT);
            ImageView taskPhoto = (ImageView) convertView.findViewById(R.id.task_photo_tasks_list);

            Bitmap bmp = BitmapFactory.decodeByteArray(photoByteSize, 0, photoByteSize.length);
            taskPhoto.setImageBitmap(bmp);
        } else {
            ImageView taskPhoto = (ImageView) convertView.findViewById(R.id.task_photo_tasks_list);
            taskPhoto.getLayoutParams().height = 0;
            taskPhoto.getLayoutParams().width = 0;
        }

        TextView pointsTV = (TextView) convertView.findViewById(R.id.tv_points);
        int points = list.get(position).getPoints();
        pointsTV.setText("$" + points);


        CircleImageView iconView = (CircleImageView) convertView.findViewById(R.id.circleView_icon);
        String iconName = list.get(position).getIconName();
        if (iconName != null && iconName.contains("icon")) {
            int resID = context.getResources().getIdentifier(iconName, "mipmap", context.getPackageName());
            if (resID != 0) {
                iconView.setImageResource(resID);
            }
        }

        ImageButton removalTask = (ImageButton) convertView.findViewById(R.id.btn_remove_task);
        removalTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                taskToRemove = list.get(position);
                showDialogRemoveTask(taskToRemove.getIdTask());
                notifyDataSetChanged();
            }
        });

        return convertView;
    }

    private void setSpeaker(int position, ImageButton speaker) {
        final String recording = list.get(position).getRecording();
        if (recording == null) {
            speaker.setVisibility(View.INVISIBLE);
        } else {
            speaker.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DownloadRecordingUtils.playSound(recording, context);
                }
            });
        }
    }

    private void showDialogRemoveTask(final int idMember) {
        if (!((Activity) context).isFinishing()) {
            sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(context.getString(R.string.dialog_remove))
                    .setContentText(context.getString(R.string.dialog_remove_task_content))
                    .setCancelText(context.getString(R.string.dialog_cancel))
                    .setConfirmText(context.getString(R.string.dialog_confirm))
                    .showCancelButton(true)
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sendRequestRemoveTask();
                        }
                    });
            sweetAlertDialog.show();
        }
    }

    private void sendRequestRemoveTask() {
        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<ResponseBody> call = apiService.removeTask(userSession.getIdMyGroup(), taskToRemove.getIdTask());
        call.enqueue(this);
    }


    @Override
    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
        if (response.code() == 200) {

            TasksFragment_ tasksFragment = new TasksFragment_();
            TasksFragment_.setContext(context);
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, tasksFragment)
                    .commit();
            DrawerActivity_.setCurrentFragment(tasksFragment);

            sweetAlertDialog
                    .setTitleText(context.getString(R.string.dialog_success))
                    .setContentText(context.getString(R.string.dialog_remove_task_success))
                    .setConfirmText(context.getString(R.string.dialog_ok))
                    .showCancelButton(false)
                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialog.show();

            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.cancel();
                }
            });

        } else {

            ResponseBody responseBody = response.errorBody();
            MessageUtils.showErrorMessagge(context, responseBody, context.getString(R.string.dialog_remove_task_fail));
            sweetAlertDialog
                    .setConfirmClickListener(null)
                    .showCancelButton(false)
                    .setConfirmText(context.getString(R.string.dialog_ok))
                    .setTitleText(context.getString(R.string.dialog_bug))
                    .setContentText(context.getString(R.string.dialog_remove_task_fail));
        }
    }

    @Override
    public void onFailure(Throwable t) {
        Toast.makeText(context, context.getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }
}
