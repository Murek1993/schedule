package pl.edu.pjatk.murek.grafik.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.ResponseBody;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.apache.commons.lang3.SerializationUtils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.fragments.AccountSettingsFragment;
import pl.edu.pjatk.murek.grafik.fragments.AccountSettingsFragment_;
import pl.edu.pjatk.murek.grafik.fragments.ApplicationInfoFragment;
import pl.edu.pjatk.murek.grafik.fragments.ApplicationInfoFragment_;
import pl.edu.pjatk.murek.grafik.fragments.JoinToPartnerGroupFragment;
import pl.edu.pjatk.murek.grafik.fragments.JoinToPartnerGroupFragment_;
import pl.edu.pjatk.murek.grafik.fragments.MembersFragment;
import pl.edu.pjatk.murek.grafik.fragments.MembersFragment_;
import pl.edu.pjatk.murek.grafik.fragments.MyGroupSettingsFragment;
import pl.edu.pjatk.murek.grafik.fragments.MyGroupSettingsFragment_;
import pl.edu.pjatk.murek.grafik.fragments.ScheduleFragment;
import pl.edu.pjatk.murek.grafik.fragments.ScheduleFragment_;
import pl.edu.pjatk.murek.grafik.fragments.ScheduleMyGroupFragment;
import pl.edu.pjatk.murek.grafik.fragments.ScheduleMyGroupFragment_;
import pl.edu.pjatk.murek.grafik.fragments.SchedulePartnerGroupFragment;
import pl.edu.pjatk.murek.grafik.fragments.SchedulePartnerGroupFragment_;
import pl.edu.pjatk.murek.grafik.fragments.SendSmsFragment;
import pl.edu.pjatk.murek.grafik.fragments.SendSmsFragment_;
import pl.edu.pjatk.murek.grafik.fragments.StatisticsFragment;
import pl.edu.pjatk.murek.grafik.fragments.StatisticsFragment_;
import pl.edu.pjatk.murek.grafik.fragments.StatisticsMyGroupFragment;
import pl.edu.pjatk.murek.grafik.fragments.StatisticsMyGroupFragment_;
import pl.edu.pjatk.murek.grafik.fragments.StatisticsPartnerGroupFragment;
import pl.edu.pjatk.murek.grafik.fragments.StatisticsPartnerGroupFragment_;
import pl.edu.pjatk.murek.grafik.fragments.TasksFragment;
import pl.edu.pjatk.murek.grafik.fragments.TasksFragment_;
import pl.edu.pjatk.murek.grafik.models.JoinToGroupData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MessageUtils;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

@EActivity
public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Callback<ResponseBody> {

    @ViewById(R.id.toolbar)
    protected Toolbar toolbar;

    @ViewById(R.id.drawer_layout)
    protected DrawerLayout drawer;

    @ViewById(R.id.nav_view)
    protected NavigationView navigationView;

    private View headerView;

    @ViewById(R.id.photo_from_session)
    protected ImageView photoFromSession;

    @ViewById(R.id.login_from_session)
    protected TextView loginFromSession;

    FragmentManager fragmentManager = getSupportFragmentManager();

    private ScheduleFragment_ scheduleFragment;
    private ScheduleMyGroupFragment_ scheduleMyGroupFragment;

    private MyGroupSettingsFragment_ myGroupSettingsFragment;
    private MembersFragment_ membersFragment;
    private TasksFragment_ tasksFragment;
    private SendSmsFragment_ sendSmsFragment;

    private JoinToPartnerGroupFragment_ joinToPartnerGroupFragment;

    private StatisticsFragment_ statisticsFragment;
    private StatisticsMyGroupFragment_ statisticsMyGroupFragment;

    private AccountSettingsFragment_ accountSettingsFragment;
    private ApplicationInfoFragment_ applicationInfoFragment;
    private SweetAlertDialog sweetAlertDialog;

    private static Fragment currentFragment;

    private static Context context;

    private static final int CODE_NFC_RECEIVE = 105;

    public static void setCurrentFragment(Fragment fragment) {
        currentFragment = fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
    }


    @AfterViews
    protected void afterViews() {
        Session session = MySharedPreferencesUtils.getSession(this);
        if (session == null) {
            goToLoginPageIfUnlogged();
        }
        context = DrawerActivity.this;
        setFragments();

        setSupportActionBar(toolbar);

        loadFragment();

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);
        Menu nav_Menu = navigationView.getMenu();
        if (session != null) {
            nav_Menu.findItem(R.id.leave_group_layout).setEnabled(session.getIdPartnerGroup() != null);
            setLoginAndPhotoFromSession(session);
        }

    }

    private void loadFragment() {
        if (currentFragment == null) {
            loadAppropriateScheduleFragment();
        } else {
            if (currentFragment instanceof ScheduleFragment_ || currentFragment instanceof ScheduleMyGroupFragment_ || currentFragment instanceof SchedulePartnerGroupFragment_) {
                loadAppropriateScheduleFragment();
            } else if (currentFragment instanceof StatisticsFragment_ || currentFragment instanceof StatisticsMyGroupFragment_ || currentFragment instanceof StatisticsPartnerGroupFragment_) {
                loadAppropriateStatisticFragment();
            } else {
                fragmentManager.beginTransaction()
                        .replace(R.id.container_drawer, currentFragment).addToBackStack(null)
                        .commit();
            }
        }
    }

    private void loadAppropriateScheduleFragment() {
        Session userSession = MySharedPreferencesUtils.getSession(this);

        if (userSession.isPartnerGroupIsActive() || userSession.isMyGroupIsActive()) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, scheduleFragment)
                    .commit();
            currentFragment = scheduleFragment;
        } else {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, scheduleMyGroupFragment)
                    .commit();
            currentFragment = scheduleMyGroupFragment;
        }
    }


    private void loadAppropriateStatisticFragment() {
        Session userSession = MySharedPreferencesUtils.getSession(this);
        if (userSession.isPartnerGroupIsActive() || userSession.isMyGroupIsActive()) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, statisticsFragment).addToBackStack(null)
                    .commit();
            currentFragment = statisticsFragment;
        } else {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, statisticsMyGroupFragment).addToBackStack(null)
                    .commit();
            currentFragment = statisticsMyGroupFragment;
        }
    }

    private void setFragments() {
        scheduleFragment = new ScheduleFragment_();
        ScheduleFragment.setContext(context);
        scheduleMyGroupFragment = new ScheduleMyGroupFragment_();
        ScheduleMyGroupFragment.setContext(context);
        SchedulePartnerGroupFragment.setContext(context);

        myGroupSettingsFragment = new MyGroupSettingsFragment_();
        MyGroupSettingsFragment.setContext(context);
        membersFragment = new MembersFragment_();
        MembersFragment.setContext(context);
        tasksFragment = new TasksFragment_();
        TasksFragment.setContext(context);
        sendSmsFragment = new SendSmsFragment_();
        SendSmsFragment.setContext(context);

        joinToPartnerGroupFragment = new JoinToPartnerGroupFragment_();
        JoinToPartnerGroupFragment.setContext(context);

        statisticsFragment = new StatisticsFragment_();
        StatisticsFragment.setContext(context);
        statisticsMyGroupFragment = new StatisticsMyGroupFragment_();
        StatisticsMyGroupFragment.setContext(context);
        StatisticsPartnerGroupFragment.setContext(context);
        accountSettingsFragment = new AccountSettingsFragment_();
        AccountSettingsFragment.setContext(context);

        applicationInfoFragment = new ApplicationInfoFragment_();
        ApplicationInfoFragment.setContext(context);
    }

    public void setLoginAndPhotoFromSession(Session session) {
        headerView = navigationView.getHeaderView(0);
        loginFromSession = (TextView) headerView.findViewById(R.id.login_from_session);
        photoFromSession = (ImageView) headerView.findViewById(R.id.photo_from_session);
        loginFromSession.setText(session.getLogin());

        if (session.getPhoto() != null) {
            SetPhoto(session);
        }
    }

    private void SetPhoto(Session session) {
        String downloadedPhoto = session.getPhoto();
        if (downloadedPhoto != null) {
            byte[] photoByteSize = Base64.decode(downloadedPhoto, Base64.DEFAULT);
            CircleImageView photo = (CircleImageView) headerView.findViewById(R.id.photo_from_session);

            Bitmap bmp = BitmapFactory.decodeByteArray(photoByteSize, 0, photoByteSize.length);
            photo.setImageBitmap(bmp);
        }
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            try {
                fragmentManager.executePendingTransactions();
                super.onBackPressed();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (currentFragment != null) {
            currentFragment = fragment;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, applicationInfoFragment).addToBackStack(null)
                    .commit();
            currentFragment = applicationInfoFragment;

        } else if (id == R.id.logout) {
            currentFragment = null;
            MySharedPreferencesUtils.clearSession(context);
            context = null;
            Intent intent = new Intent(this, MainActivity_.class);
            startActivity(intent);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.grafik_layout) {
            loadAppropriateScheduleFragment();
        } else if (id == R.id.settings_my_group_layout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, myGroupSettingsFragment).addToBackStack(null)
                    .commit();
            currentFragment = myGroupSettingsFragment;
        } else if (id == R.id.members_my_group_layout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, membersFragment).addToBackStack(null)
                    .commit();
            currentFragment = membersFragment;
        } else if (id == R.id.add_task_layout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, tasksFragment).addToBackStack(null)
                    .commit();
            currentFragment = tasksFragment;
        } else if (id == R.id.send_sms_layout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, sendSmsFragment).addToBackStack(null)
                    .commit();
            currentFragment = sendSmsFragment;
        } else if (id == R.id.join_to_partner_group_layout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, joinToPartnerGroupFragment).addToBackStack(null)
                    .commit();
            currentFragment = joinToPartnerGroupFragment;

        } else if (id == R.id.leave_group_layout) {
            showMessageForLeaveGroup();

        } else if (id == R.id.statistic_group_layout) {
            loadAppropriateStatisticFragment();
        } else if (id == R.id.account_settings_layout) {
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, accountSettingsFragment).addToBackStack(null)
                    .commit();
            currentFragment = accountSettingsFragment;
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.NFC) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.NFC}, CODE_NFC_RECEIVE);
        } else {
            receiveNFC();
        }
    }


    private void receiveNFC() {
        Intent intent = (this.getIntent());
        String action = intent.getAction();

        if (action != null && action.equals(NfcAdapter.ACTION_NDEF_DISCOVERED)) {
            Parcelable[] parcelables =
                    intent.getParcelableArrayExtra(
                            NfcAdapter.EXTRA_NDEF_MESSAGES);

            NdefMessage inNdefMessage = (NdefMessage) parcelables[0];
            NdefRecord[] inNdefRecords = inNdefMessage.getRecords();
            NdefRecord NdefRecord_0 = inNdefRecords[0];

            JoinToGroupData joinToGroupData = SerializationUtils.deserialize(NdefRecord_0.getPayload());
            if (joinToGroupData != null) {
                joinToPartnerGroupFragment.sendRequestJoinToGroup(joinToGroupData);

            }
            this.getIntent().setAction(null);
        }
    }

    public void showMessageForLeaveGroup() {
        sweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)

                .setTitleText(getString(R.string.dialog_leave_group))
                .setContentText(getString(R.string.dialog_leave_group_content))
                .setCancelText(getString(R.string.dialog_cancel))
                .setConfirmText(getString(R.string.dialog_confirm))
                .showCancelButton(true)
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.cancel();
                    }
                }).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sendRequestLeaveGroup();

                    }
                });
        sweetAlertDialog.show();
    }

    public void sendRequestLeaveGroup() {

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession(this);
        Call<ResponseBody> call = apiService.leaveGroup(userSession.getIdPartnerGroup(), userSession.getIdAccount());
        call.enqueue(this);
    }


    @Override
    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
        if (response.code() == 200) {
            Toast.makeText(context, R.string.dialog_leave_group_success, Toast.LENGTH_LONG).show();
            Session session = MySharedPreferencesUtils.getSession(this);
            session.setIdPartnerGroup(null);
            session.setPartnerGroupIsActive(false);
            MySharedPreferencesUtils.saveSession(session, this);

            sweetAlertDialog
                    .setTitleText(getString(R.string.dialog_success))
                    .setContentText(getString(R.string.leave_group_success))
                    .setConfirmText(getString(R.string.dialog_ok))
                    .showCancelButton(false)
                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            sweetAlertDialog.show();

            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.cancel();
                    restartDrawerActivity();

                }
            });

        } else {

            ResponseBody responseBody = response.errorBody();
            MessageUtils.showErrorMessagge(context, responseBody, getString(R.string.dialog_leave_group_fail));
            sweetAlertDialog
                    .setConfirmClickListener(null)
                    .showCancelButton(false)
                    .setConfirmText(getString(R.string.dialog_ok))
                    .setTitleText(getString(R.string.dialog_bug))
                    .setContentText(getString(R.string.dialog_leave_group_fail));
        }
    }

    @Override
    public void onFailure(Throwable t) {
        Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }

    public void restartDrawerActivity() {
        Intent intent = getIntent();
        currentFragment = null;
        finish();
        startActivity(intent);
        context = DrawerActivity.this;
        setFragments();
        loadAppropriateScheduleFragment();
    }

    public Toolbar getToolbar() {
        return toolbar;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, getString(R.string.toast_no_permissions), Toast.LENGTH_SHORT).show();
        } else if (requestCode == CODE_NFC_RECEIVE) {
            receiveNFC();
        } else {
            currentFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void goToLoginPageIfUnlogged() {
        if (!MySharedPreferencesUtils.isLogged(DrawerActivity.this)) {
            Intent intent = new Intent(DrawerActivity.this, MainActivity_.class);
            startActivity(intent);
            this.finish();
        }
    }
}
