package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */

@Getter
@Setter
public class TaskDataWithStatus {

    @SerializedName("StatusWykonania")
    public int performedStatus;
    @SerializedName("StatusWeryfikacji")
    public Integer verificationStatus;
    @SerializedName("IdSprzataniaZadania")
    public int idCleaningTask;
    @SerializedName("Nazwa")
    private String name;
    @SerializedName("Opis")
    private String description;
    @SerializedName("Zdjecie")
    private String photo;
    @SerializedName("NazwaIkony")
    private String iconName;
    @SerializedName("Nagranie")
    private String recording;
    @SerializedName("Punkty")
    private int points;

    public TaskDataWithStatus(String name, String description, String photo, String iconName, String recording,
                              int points, int performedStatus, Integer verificationStatus, int idCleaningTask) {
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.iconName = iconName;
        this.recording = recording;
        this.points = points;
        this.performedStatus = performedStatus;
        this.verificationStatus = verificationStatus;
        this.idCleaningTask = idCleaningTask;
    }
}
