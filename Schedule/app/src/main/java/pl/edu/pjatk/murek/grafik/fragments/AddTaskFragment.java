package pl.edu.pjatk.murek.grafik.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.squareup.okhttp.ResponseBody;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.ViewById;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import lombok.Setter;
import pl.edu.pjatk.murek.grafik.R;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity;
import pl.edu.pjatk.murek.grafik.activities.DrawerActivity_;
import pl.edu.pjatk.murek.grafik.api.ApiController;
import pl.edu.pjatk.murek.grafik.api.ApiService;
import pl.edu.pjatk.murek.grafik.models.TaskToEditData;
import pl.edu.pjatk.murek.grafik.models.Session;
import pl.edu.pjatk.murek.grafik.utils.MySharedPreferencesUtils;
import pl.edu.pjatk.murek.grafik.utils.DownloadRecordingUtils;
import pl.edu.pjatk.murek.grafik.utils.DownloadedPhotosUtils;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created on 2017-04-19.
 */
@Setter
@EFragment(R.layout.add_task_layout)
public class AddTaskFragment extends Fragment implements Callback<ResponseBody> {

    private static final int CODE_PHOTO_FROM_CAMERA = 1;
    private static final int CODE_PHOTO_FROM_GALLERY = 2;
    private static final int CODE_RECORDING = 3;
    private static final int CODE_ICON = 4;
    @ViewById(R.id.et_task_name)
    protected EditText taskNameET;
    @ViewById(R.id.et_photo)
    protected EditText photoET;
    @ViewById(R.id.et_icon)
    protected EditText iconET;
    @ViewById(R.id.et_recording)
    protected EditText recordingET;
    @ViewById(R.id.et_description)
    protected EditText descriptionET;
    @ViewById(R.id.et_points)
    protected EditText pointsET;
    @ViewById(R.id.add_task)
    protected Button addTaskButton;
    @ViewById(R.id.progress_view_add_task)
    protected CircularProgressView progressView;

    private static Context context;
    private FragmentManager fragmentManager;
    private String icon = "";
    private String photoPath = "";
    private String recordingPath = "";

    public static void setContext(Context context) {
        AddTaskFragment.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.add_task_layout, container, false);
    }

    @AfterViews
    protected void afterViews() {
        ((DrawerActivity) context).getToolbar().setTitle(R.string.add_task);
        fragmentManager = ((DrawerActivity) context).getSupportFragmentManager();
    }

    @Click(R.id.add_task)
    protected void addTaskClick() {
        String taskName = taskNameET.getText().toString();
        String valueFieldPoints = pointsET.getText().toString();
        String iconName = iconET.getText().toString();
        if (ifCorrectData(taskName, valueFieldPoints, iconName)) {
            addTask(taskName, Integer.parseInt(valueFieldPoints));
        }

    }

    private void addTask(String taskName, int points) {
        String description = descriptionET.getText().toString();
        String photo = null;
        String photoName = photoET.getText().toString();

        if (!photoName.equals("") && !photoPath.equals("")) {
            photo = downloadPhotoData(photoPath);
        }
        String recording = null;
        String recordingName = recordingET.getText().toString();
        if (!recordingName.equals("") && !recordingPath.equals("")) {
            recording = downloadRecordingData(recordingPath);
        }

        String iconName = ((iconET.getText().toString().equals("")) ? null : iconET.getText().toString());

        TaskToEditData newTask = new TaskToEditData(taskName, description, photo, iconName, recording, points);
        sendRequest(newTask);
    }


    private String downloadRecordingData(String recordingPath) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(recordingPath));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n;
            while (-1 != (n = fis.read(buf))) {
                baos.write(buf, 0, n);
            }
            byte[] recordingByte = baos.toByteArray();
            return Base64.encodeToString(recordingByte, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        finally {
            if(fis != null){
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

    }

    private String downloadPhotoData(String photoPath) {

        Bitmap bitmap = DownloadedPhotosUtils.getThumbnailBitmap(photoPath, 400);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] photoByte = stream.toByteArray();
        return Base64.encodeToString(photoByte, Base64.DEFAULT);
    }
    
    private boolean ifCorrectData(String TaskName, String valueFieldPoints, String iconName) {
        if (TaskName.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_task_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (valueFieldPoints.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_points), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (Integer.parseInt(valueFieldPoints) < 1) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_points_must_bigger_than_zero), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (iconName.equals("")) {
            Toast.makeText(context, getString(R.string.toast_walidation_fill_field_icon), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void sendRequest(TaskToEditData newTask) {
        progressView.startAnimation();
        progressView.setVisibility(View.VISIBLE);
        addTaskButton.setEnabled(false);

        ApiService apiService = new ApiController(context).getAPIServiceWithAuthorization();
        Session userSession = MySharedPreferencesUtils.getSession((Activity) context);
        Call<ResponseBody> call = apiService.addTask(newTask, userSession.getIdMyGroup());
        call.enqueue(this);
    }

    @FocusChange(R.id.et_recording)
    void focusNaNagranie(View recording, boolean hasFocus) {
        if (hasFocus) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.RECORD_AUDIO}, CODE_RECORDING);
            } else {
               record();
            }
        }
    }

    private void record(){
        Intent recordIntent = new Intent(
                MediaStore.Audio.Media.RECORD_SOUND_ACTION);
        startActivityForResult(recordIntent, CODE_RECORDING);
    }

    @FocusChange(R.id.et_icon)
    void focusNaIkone(View icon, boolean hasFocus) {
        if (hasFocus) {
            GridViewChoiceIconFragment_ iconGallery = new GridViewChoiceIconFragment_();
            iconGallery.setContext(context);
            iconGallery.setTargetFragment(this, CODE_ICON);
            fragmentManager.beginTransaction()
                    .replace(R.id.container_drawer, iconGallery).addToBackStack(null)
                    .commit();
            DrawerActivity_.setCurrentFragment(iconGallery);
        }
    }

    @FocusChange(R.id.et_photo)
    void focusOnPhoto(View photo, boolean hasFocus) {
        if (hasFocus) {
            final CharSequence[] elements = {getString(R.string.take_photo), getString(R.string.select_from_gallery), getString(R.string.cancel)};
            if (!((Activity) context).isFinishing()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                LayoutInflater inflater = ((DrawerActivity) context).getLayoutInflater();

                View heading = inflater.inflate(R.layout.menu_photo_title_layout, null);
                builder.setCustomTitle(heading);
                builder.setItems(elements, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (elements[item].equals(elements[0])) {
                            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, CODE_PHOTO_FROM_CAMERA);
                            } else {
                                takePhoto();
                            }
                        } else if (elements[item].equals(elements[1])) {
                            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE_PHOTO_FROM_GALLERY);
                            } else {
                                getPhotoFromGalerry();
                            }
                        } else if (elements[item].equals(elements[2])) {
                            dialog.dismiss();
                        }
                    }
                });

                AlertDialog alertDialog = builder.create();
                ListView listView = alertDialog.getListView();
                listView.setDivider(new ColorDrawable(Color.BLACK));
                listView.setDividerHeight(1);
                listView.setOverscrollFooter(new ColorDrawable(Color.TRANSPARENT));
                listView.setHeaderDividersEnabled(true);

                alertDialog.show();
            }
        }
    }

    private void takePhoto(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CODE_PHOTO_FROM_CAMERA);
    }

    private void getPhotoFromGalerry(){
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, CODE_PHOTO_FROM_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (data != null && resultCode == Activity.RESULT_OK) {
                if (requestCode == CODE_PHOTO_FROM_CAMERA) {
                    photoPath = DownloadedPhotosUtils.takePhoto(data);
                    String[] fileName = photoPath.split("/");
                    photoET.setText(fileName[fileName.length - 1]);
                    descriptionET.requestFocus();
                } else if (requestCode == CODE_PHOTO_FROM_GALLERY) {
                    photoPath = DownloadedPhotosUtils.takePhotoFromGallery(data, ((DrawerActivity) context));
                    String[] fileName = photoPath.split("/");
                    photoET.setText(fileName[fileName.length - 1]);
                    descriptionET.requestFocus();
                } else if (requestCode == CODE_RECORDING) {
                    recordingPath = DownloadRecordingUtils.getRecording(data, ((DrawerActivity) context));
                    String[] fileName = recordingPath.split("/");
                    recordingET.setText(fileName[fileName.length - 1]);
                    descriptionET.requestFocus();
                } else if (requestCode == CODE_ICON) {
                    String iconTitle = data.getStringExtra("iconTitle");
                    icon = iconTitle;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disableRefreshEffect() {

        progressView.stopAnimation();
        progressView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
        disableRefreshEffect();
        addTaskButton.setEnabled(true);

        if (response.code() == 200) {
            Toast.makeText(context, getString(R.string.toast_add_task_success), Toast.LENGTH_SHORT).show();
            clearForm();
        } else {
            Toast.makeText(context, getString(R.string.toast_add_task_fail), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        disableRefreshEffect();
        addTaskButton.setEnabled(true);

        Toast.makeText(context, getString(R.string.toast_server_fail), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onViewStateRestored(Bundle inState) {
        super.onViewStateRestored(inState);
        iconET.setText(icon);
    }

    private void clearForm() {
        taskNameET.getText().clear();
        photoET.getText().clear();
        iconET.getText().clear();
        recordingET.getText().clear();
        descriptionET.getText().clear();
        pointsET.getText().clear();
        taskNameET.requestFocus();
        icon = "";
        photoPath = "";
        recordingPath = "";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        if (requestCode == CODE_RECORDING) {
            record();
        } else if (requestCode == CODE_PHOTO_FROM_CAMERA) {
            takePhoto();
        }else if(requestCode == CODE_PHOTO_FROM_GALLERY){
            getPhotoFromGalerry();
        }
    }

}
