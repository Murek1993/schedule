package pl.edu.pjatk.murek.grafik.models;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

/**
 * Created on 2017-04-18.
 */
@Getter
@Setter
public class SettingsAccountData {

    @SerializedName("StareHaslo")
    private String oldPassword;

    @SerializedName("NoweHaslo")
    private String newPassword;

    @SerializedName("PowtorzHaslo")
    private String repeatPassword;

    @SerializedName("Zdjecie")
    private String photo;

    public SettingsAccountData(String oldPassword, String newPassword, String repeatPassword, String photo) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
        this.repeatPassword = repeatPassword;
        this.photo = photo;
    }
}
